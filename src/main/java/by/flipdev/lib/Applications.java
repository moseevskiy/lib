package by.flipdev.lib;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Applications {
    private ActivityManager mActivityManager;
//    public static List<PackageInfo> getInstalledApps(Context context, boolean getSysPackages) {
//        List<PackageInfo> allApps = context.getPackageManager().getInstalledPackages(0);
//        if(!getSysPackages){
//            final List<PackageInfo> apps = new ArrayList<PackageInfo>();
//            for (int i = allApps.size()-1; i >= 0; i--) {
//                PackageInfo p = allApps.get(i);
//                if (p.versionName == null) {
//                    apps.add(allApps.get(i));
//                }
//                return apps;
//            }
//        }
//
//        return allApps;
//    }

    public static List<PackageInfo> getInstalledApps(Context context, boolean getSysPackages) {
        List<PackageInfo> allApps = context.getPackageManager().getInstalledPackages(0);
        if (!getSysPackages) {
            final List<PackageInfo> apps = new ArrayList<PackageInfo>();
            for (int i = allApps.size() - 1; i >= 0; i--) {
                PackageInfo p = allApps.get(i);
                if (p.versionName == null) {
                    apps.add(allApps.get(i));
                }
                return apps;
            }
        }

        return allApps;
    }
//
//    public static void killApp(Context context, boolean getSysPackages) {
//        List<PackageInfo> allApps = context.getPackageManager().getInstalledPackages(0);
//        if(!getSysPackages){
//            final List<PackageInfo> apps = new ArrayList<PackageInfo>();
//            for (int i = allApps.size()-1; i >= 0; i--) {
//                PackageInfo p = allApps.get(i);
//                if (p.versionName == null) {
//                    apps.add(allApps.get(i));
//                }
//                return apps;
//            }
//        }
//
//        return allApps;
//    }

    public static ActivityManager.RunningAppProcessInfo getForegroundApp(Context context) {
        ActivityManager.RunningAppProcessInfo result = null, info = null;

        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> l = mActivityManager.getRunningAppProcesses();
        Iterator<ActivityManager.RunningAppProcessInfo> i = l.iterator();
        while (i.hasNext()) {
            info = i.next();
            if (info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
                    && !isRunningService(context, info.processName)) {
                result = info;
                break;
            }
        }
        return result;
    }

    public static ComponentName getActivityForApp(Context context, ActivityManager.RunningAppProcessInfo target) {
        ComponentName result = null;
        ActivityManager.RunningTaskInfo info;

        if (target == null)
            return null;

        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> l = mActivityManager.getRunningTasks(9999);
        Iterator<ActivityManager.RunningTaskInfo> i = l.iterator();

        while (i.hasNext()) {
            info = i.next();
            if (info.baseActivity.getPackageName().equals(target.processName)) {
                result = info.topActivity;
                break;
            }
        }

        return result;
    }

    public static boolean isStillActive(Context context, ActivityManager.RunningAppProcessInfo process, ComponentName activity) {
        // activity can be null in cases, where one app starts another. for example, astro
        // starting rock player when a move file was clicked. we dont have an activity then,
        // but the package exits as soon as back is hit. so we can ignore the activity
        // in this case
        if (process == null)
            return false;

        ActivityManager.RunningAppProcessInfo currentFg = getForegroundApp(context);
        ComponentName currentActivity = getActivityForApp(context, currentFg);

        if (currentFg != null && currentFg.processName.equals(process.processName) &&
                (activity == null || currentActivity.compareTo(activity) == 0))
            return true;


        return false;
    }

    public static boolean isRunningService(Context context, String packageName) {
        if (packageName == null || packageName.isEmpty())
            return false;

        ActivityManager.RunningServiceInfo service;

        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> l = mActivityManager.getRunningServices(9999);
        Iterator<ActivityManager.RunningServiceInfo> i = l.iterator();
        while (i.hasNext()) {
            service = i.next();
            if (service.process.equals(packageName))
                return true;
        }

        return false;
    }
}
