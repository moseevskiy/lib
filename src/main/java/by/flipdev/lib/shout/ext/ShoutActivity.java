package by.flipdev.lib.shout.ext;

import android.app.Activity;
import android.os.Bundle;

import by.flipdev.lib.shout.Shout;


/**
 * Created by Flip on 17.11.13.
 */
public class ShoutActivity extends Activity {
    Shout shout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (shout != null)
            shout.stop();
    }

    public void registerShout() {
        shout = Shout.create(this);
    }

    public void registerShout(String uniqId) {
        shout = Shout.create(this, uniqId);
    }

    public Shout getShout() {
        if (shout == null)
            shout = Shout.create(this);
        return shout;
    }

}
