//package by.flipdev.lib.shout.ext;
//
//import android.support.v4.app.ListFragment;
//
//import by.flipdev.lib.shout.Shout;
//
//
///**
// * Created by Flip on 17.11.13.
// */
//public class ShoutSupportListFragment extends ListFragment {
//    Shout shout;
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (shout != null)
//            shout.stop();
//    }
//
//    public void registerShout() {
//        shout = Shout.create(getActivity());
//    }
//
//    public void registerShout(String uniqId) {
//        shout = Shout.create(getActivity(), uniqId);
//    }
//
//    public Shout getShout() {
//        if (shout == null)
//            shout = Shout.create(getActivity());
//        return shout;
//    }
//
//}
