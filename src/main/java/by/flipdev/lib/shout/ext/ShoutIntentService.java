package by.flipdev.lib.shout.ext;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import by.flipdev.lib.shout.Shout;
import by.flipdev.lib.shout.listeners.CommandListener;
import by.flipdev.lib.shout.listeners.StringListener;

/**
 * Created by egormosevskiy on 16.01.14.
 */
public class ShoutIntentService extends IntentService {

    public ShoutIntentService(String tag) {
        super(tag);
    }

    public static Shout setCommandListener(Context context, String tag, CommandListener commandListener) {
        return Shout.create(context, tag, commandListener);
    }

    public static Shout setStringListener(Context context, String tag, StringListener stringListener) {
        return Shout.create(context, tag, stringListener);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }
}
