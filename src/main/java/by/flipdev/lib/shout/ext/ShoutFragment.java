package by.flipdev.lib.shout.ext;


import android.app.Fragment;

import by.flipdev.lib.shout.Shout;


/**
 * Created by Flip on 17.11.13.
 */
public class ShoutFragment extends Fragment {
    Shout shout;

    @Override
    public void onDestroy() {
        super.onDestroy();
        stop();
    }

    public void registerShout() {
        shout = Shout.create(getActivity());
    }

    public void registerShout(String uniqId) {
        stop();
        shout = Shout.create(getActivity(), uniqId);
    }

    private void stop() {
        if (shout != null) {
            shout.stop();
            shout = null;
        }
    }

    public Shout getShout() {
        if (shout == null)
            shout = Shout.create(getActivity());
        return shout;
    }

}
