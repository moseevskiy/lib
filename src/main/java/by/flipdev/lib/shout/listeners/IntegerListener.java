package by.flipdev.lib.shout.listeners;

import java.util.EventListener;

public interface IntegerListener extends EventListener {
    void onReceiveInteger(int command, int value);
}