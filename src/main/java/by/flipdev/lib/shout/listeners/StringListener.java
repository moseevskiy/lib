package by.flipdev.lib.shout.listeners;

import java.util.EventListener;

public interface StringListener extends EventListener {
    void onReceiveString(int command, String value);
}