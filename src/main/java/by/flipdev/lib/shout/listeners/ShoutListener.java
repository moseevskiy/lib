package by.flipdev.lib.shout.listeners;

import java.util.EventListener;

public interface ShoutListener extends EventListener {
    void onReceiveShout();
}