package by.flipdev.lib.shout.listeners;

import android.os.Bundle;

import java.util.EventListener;

public interface BundleListener extends EventListener {
    void onReceiveBundle(Bundle bundle);
}