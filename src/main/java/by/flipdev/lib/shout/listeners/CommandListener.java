package by.flipdev.lib.shout.listeners;

import java.util.EventListener;

public interface CommandListener extends EventListener {
    void onReceiveCommand(int command);
}