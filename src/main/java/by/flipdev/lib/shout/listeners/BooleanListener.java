package by.flipdev.lib.shout.listeners;

import java.util.EventListener;

public interface BooleanListener extends EventListener {
    void onReceiveBoolean(int command, boolean value);
}