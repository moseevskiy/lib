/*******************************************************************************
 * Copyright 2012 Egor Moseevskiy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package by.flipdev.lib.shout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import by.flipdev.lib.shout.listeners.BooleanListener;
import by.flipdev.lib.shout.listeners.BundleListener;
import by.flipdev.lib.shout.listeners.CommandListener;
import by.flipdev.lib.shout.listeners.IntegerListener;
import by.flipdev.lib.shout.listeners.ShoutListener;
import by.flipdev.lib.shout.listeners.StringListener;


public class Shout extends BroadcastReceiver {
    private static final String COMMAND = "COMMAND";
    private static final String COMMAND_INT_VALUE = "COMMAND_INT_VALUE";
    private static final String COMMAND_STRING_VALUE = "COMMAND_STRING_VALUE";
    private static final String COMMAND_BOOLEAN_VALUE = "COMMAND_BOOLEAN_VALUE";
    private static final String SHOUT = "SHOUT";

    private static final String BUNDLE = "BUNDLE";


    private String intentId;
    private String uniqId = null;
    private Context context;
    private ShoutListener shoutListener = null;
    private CommandListener commandListener = null;
    private BooleanListener booleanListener = null;
    private IntegerListener integerListener = null;
    private StringListener stringListener = null;
    private BundleListener bundleListener = null;

    private Shout(Context context, String uniqId,
                  ShoutListener shoutListener,
                  CommandListener commandListener,
                  BooleanListener booleanListener,
                  IntegerListener integerListener,
                  StringListener stringListener,
                  BundleListener bundleListener) {
        this.context = context;
        this.uniqId = uniqId;
        this.shoutListener = shoutListener;
        this.commandListener = commandListener;
        this.booleanListener = booleanListener;
        this.integerListener = integerListener;
        this.stringListener = stringListener;
        this.bundleListener = bundleListener;
        regReceiver();
    }

    private Shout() {

    }

    public static Shout create(Context context) {
        return new Shout(context,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               ShoutListener shoutListener) {
        return new Shout(context,
                null,
                shoutListener,
                null,
                null,
                null,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               CommandListener commandListener) {
        return new Shout(context,
                null,
                null,
                commandListener,
                null,
                null,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               BooleanListener booleanListener) {
        return new Shout(context,
                null,
                null,
                null,
                booleanListener,
                null,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               IntegerListener integerListener) {
        return new Shout(context,
                null,
                null,
                null,
                null,
                integerListener,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               StringListener stringListener) {
        return new Shout(context,
                null,
                null,
                null,
                null,
                null,
                stringListener,
                null
        );
    }

    public static Shout create(Context context,
                               BundleListener bundleListener) {
        return new Shout(context,
                null,
                null,
                null,
                null,
                null,
                null,
                bundleListener
        );
    }

    public static Shout create(Context context,
                               String uniqId) {
        return new Shout(context,
                uniqId,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               String uniqId,
                               ShoutListener shoutListener) {
        return new Shout(context,
                uniqId,
                shoutListener,
                null,
                null,
                null,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               String uniqId,
                               CommandListener commandListener) {
        return new Shout(context,
                uniqId,
                null,
                commandListener,
                null,
                null,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               String uniqId,
                               BooleanListener booleanListener) {
        return new Shout(context,
                uniqId,
                null,
                null,
                booleanListener,
                null,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               String uniqId,
                               IntegerListener integerListener) {
        return new Shout(context,
                uniqId,
                null,
                null,
                null,
                integerListener,
                null,
                null
        );
    }

    public static Shout create(Context context,
                               String uniqId,
                               StringListener stringListener) {
        return new Shout(context,
                uniqId,
                null,
                null,
                null,
                null,
                stringListener,
                null
        );
    }

    public static Shout create(Context context,
                               String uniqId,
                               BundleListener bundleListener) {
        return new Shout(context,
                uniqId,
                null,
                null,
                null,
                null,
                null,
                bundleListener
        );
    }

    public static void sendCommand(Context context, String uniqId, int command) {
        context.sendBroadcast(getCommandIntent(context, command, uniqId));
    }

    public static void sendInteger(Context context, String uniqId, int command, int commandIntValue) {
        context.sendBroadcast(getCommandIntent(context, command, uniqId)
                        .putExtra(COMMAND_INT_VALUE, commandIntValue)
        );
    }

    public static void sendBoolean(Context context, String uniqId, int command, Boolean commandBooleanValue) {
        context.sendBroadcast(getCommandIntent(context, command, uniqId)
                        .putExtra(COMMAND_BOOLEAN_VALUE, commandBooleanValue)
        );
    }

    public static void sendString(Context context, String uniqId, int command, String commandStringValue) {
        context.sendBroadcast(getCommandIntent(context, command, uniqId)
                        .putExtra(COMMAND_STRING_VALUE, commandStringValue)
        );
    }

    public static void sendShout(Context context, String uniqId) {
        context.sendBroadcast(new Intent(getIntentId(context, uniqId))
                        .putExtra(SHOUT, true)
        );
    }

    public static void sendCommand(Context context, int command) {
        context.sendBroadcast(getCommandIntent(context, command, null));
    }

    public static void sendInteger(Context context, int command, int commandIntValue) {
        context.sendBroadcast(getCommandIntent(context, command, null)
                        .putExtra(COMMAND_INT_VALUE, commandIntValue)
        );
    }

    public static void sendBoolean(Context context, int command, Boolean commandBooleanValue) {
        context.sendBroadcast(getCommandIntent(context, command, null)
                        .putExtra(COMMAND_BOOLEAN_VALUE, commandBooleanValue)
        );
    }

    public static void sendString(Context context, int command, String commandStringValue) {
        context.sendBroadcast(getCommandIntent(context, command, null)
                        .putExtra(COMMAND_STRING_VALUE, commandStringValue)
        );
    }

    public static void sendShout(Context context) {
        context.sendBroadcast(new Intent(getIntentId(context))
                        .putExtra(SHOUT, true)
        );
    }

    public static void sendBundle(Context context, Bundle bundle) {
        Intent intent = getIntent(context);
        intent.putExtra(BUNDLE, bundle);
        context.sendBroadcast(intent);
    }

    public static void sendBundle(Context context, String uniqId, Bundle bundle) {
        Intent intent = getIntent(context, uniqId);
        intent.putExtra(BUNDLE, bundle);
        context.sendBroadcast(intent);
    }

    private static Intent getCommandIntent(Context context, int command, String uniqId) {
        String intentId = getIntentId(context, uniqId);
        Intent intent = new Intent(intentId);
        intent.putExtra(COMMAND, command);
        return intent;
    }

    public static String getIntentId(Context context, String uniqId) {
        if (uniqId != null)
            return context.getPackageName() + "." + uniqId + "." + COMMAND;
        else return context.getPackageName() + "." + COMMAND;
    }

    public static String getIntentId(Context context) {
        return getIntentId(context, null);
    }

    public static Intent getIntent(Context context, String uniqId) {
        return new Intent(getIntentId(context, uniqId));
    }

    public static Intent getIntent(Context context) {
        return getIntent(context, null);
    }

    public Shout setListener(CommandListener commandListener) {
        this.commandListener = commandListener;
        return this;
    }

    public Shout setListener(IntegerListener integerListener) {
        this.integerListener = integerListener;
        return this;
    }

    public Shout setListener(BooleanListener booleanListener) {
        this.booleanListener = booleanListener;
        return this;
    }

    public Shout setListener(StringListener stringListener) {
        this.stringListener = stringListener;
        return this;
    }

    public Shout setListener(BundleListener bundleListener) {
        this.bundleListener = bundleListener;
        return this;
    }

    public Shout setListener(ShoutListener shoutListener) {
        this.shoutListener = shoutListener;
        return this;
    }

    private void regReceiver() {
        intentId = getIntentId(context, uniqId);
        final IntentFilter intFilt = new IntentFilter();
        intFilt.addAction(intentId);
        context.registerReceiver(this, intFilt);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras.containsKey(SHOUT)) {
            if (shoutListener != null)
                shoutListener.onReceiveShout();
            return;
        }
        if (extras.containsKey(COMMAND) && !extras.containsKey(COMMAND_INT_VALUE) && !extras.containsKey(COMMAND_STRING_VALUE) && !extras.containsKey(COMMAND_BOOLEAN_VALUE) && !extras.containsKey(SHOUT)) {
            if (commandListener != null)
                commandListener.onReceiveCommand(extras.getInt(COMMAND));
            return;
        }
        if (extras.containsKey(COMMAND) && extras.containsKey(COMMAND_INT_VALUE)) {
            if (integerListener != null)
                integerListener.onReceiveInteger(extras.getInt(COMMAND), extras.getInt(COMMAND_INT_VALUE));
            return;
        }
        if (extras.containsKey(COMMAND) && extras.containsKey(COMMAND_STRING_VALUE)) {
            if (stringListener != null)
                stringListener.onReceiveString(extras.getInt(COMMAND), extras.getString(COMMAND_STRING_VALUE));
            return;
        }
        if (extras.containsKey(COMMAND) && extras.containsKey(COMMAND_BOOLEAN_VALUE)) {
            if (booleanListener != null)
                booleanListener.onReceiveBoolean(extras.getInt(COMMAND), extras.getBoolean(COMMAND_BOOLEAN_VALUE));
            return;
        }
        if (extras.containsKey(BUNDLE)) {
            if (bundleListener != null)
                bundleListener.onReceiveBundle(intent.getExtras().getBundle(BUNDLE));
            return;
        }

    }

    public void stop() {
        context.unregisterReceiver(this);
    }

    public String getIntentId() {
        return intentId;
    }

    public String getUniqId() {
        return uniqId;
    }
}
