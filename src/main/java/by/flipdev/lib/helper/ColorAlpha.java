package by.flipdev.lib.helper;

import android.graphics.Color;

/**
 * Created by egormoseevskiy on 14.05.14.
 */
                /*
100% — FF
95% — F2
90% — E6
85% — D9
80% — CC
75% — BF
70% — B3
65% — A6
60% — 99
55% — 8C
50% — 80
45% — 73
40% — 66
35% — 59
30% — 4D
25% — 40
20% — 33
15% — 26
10% — 1A
5% — 0D
0% — 00
 */

public class ColorAlpha {

    public static String getParcebleColor(String colorString, Percent percent) {
        try {
            Color.parseColor(colorString);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IsNotAColorException(colorString);
        }
        String alphaHex = "00";
        switch (percent) {
            case ZERO:
                alphaHex = "00";
                break;
            case FIVE:
                alphaHex = "0D";
                break;
            case TEN:
                alphaHex = "1A";
                break;
            case FIFTEEN:
                alphaHex = "26";
                break;
            case TWENTY:
                alphaHex = "33";
                break;
            case TWENTY_FIVE:
                alphaHex = "40";
                break;
            case THIRTY:
                alphaHex = "4D";
                break;
            case THIRTY_FIVE:
                alphaHex = "59";
                break;
            case FORTY:
                alphaHex = "66";
                break;
            case FORTY_FIVE:
                alphaHex = "33";
                break;
            case FIFTY:
                alphaHex = "80";
                break;
            case FIFTY_FIVE:
                alphaHex = "8C";
                break;
            case SIXTY:
                alphaHex = "99";
                break;
            case SIXTY_FIVE:
                alphaHex = "A6";
                break;
            case SEVENTY:
                alphaHex = "B3";
                break;
            case SEVENTY_FIVE:
                alphaHex = "BF";
                break;
            case EIGHTY:
                alphaHex = "CC";
                break;
            case EIGHTY_FIVE:
                alphaHex = "D9";
                break;
            case NINETY:
                alphaHex = "E6";
                break;
            case NINETY_FIVE:
                alphaHex = "F2";
                break;
            case HUNDRED:
                alphaHex = "FF";
                break;
        }
        if (colorString.length() == 7) {
            return "#" + alphaHex + colorString.substring(1);
        }
        if (colorString.length() == 9) {
            return "#" + alphaHex + colorString.substring(3);
        }

        return null;
    }

    private static String intToHex(int i) {
        return Integer.toHexString(i);
    }

    public enum Percent {
        ZERO, FIVE, TEN, FIFTEEN, TWENTY, TWENTY_FIVE, THIRTY, THIRTY_FIVE, FORTY, FORTY_FIVE, FIFTY, FIFTY_FIVE, SIXTY, SIXTY_FIVE, SEVENTY, SEVENTY_FIVE, EIGHTY, EIGHTY_FIVE, NINETY, NINETY_FIVE, HUNDRED;
    }

    public static class IsNotAColorException extends RuntimeException {
        public IsNotAColorException(String color) {
            super("String " + color + "can't be coast as parceble color");
        }
    }
}
