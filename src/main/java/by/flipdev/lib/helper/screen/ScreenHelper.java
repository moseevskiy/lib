package by.flipdev.lib.helper.screen;

import android.content.Context;
import android.os.PowerManager;

/**
 * Created by egormosevskiy on 05.12.13.
 */
public class ScreenHelper {
    public static void wake(Context context) {
        PowerManager TempPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock TempWakeLock = TempPowerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.ON_AFTER_RELEASE, "TempWakeLock");
        TempWakeLock.acquire();
        TempWakeLock.release();
    }
}
