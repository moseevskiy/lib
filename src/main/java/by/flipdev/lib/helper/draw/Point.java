package by.flipdev.lib.helper.draw;

public class Point {

    public float x = 0;
    public float y = 0;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }
}