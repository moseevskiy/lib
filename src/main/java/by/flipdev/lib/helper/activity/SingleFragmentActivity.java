package by.flipdev.lib.helper.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public abstract class SingleFragmentActivity extends Activity {

    public static final String FRAGMENT_TAG = "single";
    private Fragment fragment;

    public static void start(Context context, Class cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            fragment = onCreateFragment();
            getFragmentManager().beginTransaction()
                    .add(android.R.id.content, fragment, FRAGMENT_TAG)
                    .commit();
        } else {
            fragment = getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        }
    }

    public abstract Fragment onCreateFragment();

    public Fragment getFragment() {
        return fragment;
    }

}
