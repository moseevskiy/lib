package by.flipdev.lib.helper.cursor;

import android.database.Cursor;

public class CursorHelper {
    Cursor cursor;

    private CursorHelper(Cursor cursor) {
        this.cursor = cursor;
    }

    public static CursorHelper create(Cursor cursor) {
        return new CursorHelper(cursor);
    }

    public static String getString(Cursor cursor, String columnName) {
        int colIndex = cursor.getColumnIndex(columnName);
        return cursor.getString(colIndex);
    }

    public static Integer getInt(Cursor cursor, String columnName) {
        int colIndex = cursor.getColumnIndex(columnName);
        return cursor.getInt(colIndex);
    }

    public static long getLong(Cursor cursor, String columnName) {
        int colIndex = cursor.getColumnIndex(columnName);
        return cursor.getLong(colIndex);
    }

    public static Double getDouble(Cursor cursor, String columnName) {
        int colIndex = cursor.getColumnIndex(columnName);
        return cursor.getDouble(colIndex);
    }

    public static Boolean getBoolean(Cursor cursor, String columnName) {
        int colIndex = cursor.getColumnIndex(columnName);
        if (cursor.getInt(colIndex) == 0)
            return false;
        else
            return true;
    }

    public String getString(String columnName) {
        return getString(cursor, columnName);
    }

    public Integer getInt(String columnName) {
        return getInt(cursor, columnName);
    }

    public Double getDouble(String columnName) {
        int colIndex = cursor.getColumnIndex(columnName);
        return cursor.getDouble(colIndex);
    }

    public Boolean getBoolean(String columnName) {
        return getBoolean(cursor, columnName);
    }

    public long getLong(String columnName) {
        int colIndex = cursor.getColumnIndex(columnName);
        return cursor.getLong(colIndex);
    }


}
