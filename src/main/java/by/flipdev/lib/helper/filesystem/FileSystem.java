package by.flipdev.lib.helper.filesystem;

import android.app.Activity;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class FileSystem {


    static public boolean delDir(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    delDir(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    public static void delFile(final String Dir_On_SD, final String File_Name) {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
        }
        try {
            File sdPath = Environment.getExternalStorageDirectory();
            sdPath = new File(sdPath.getAbsolutePath() + "/" + Dir_On_SD);
            final File sdFile = new File(sdPath, File_Name);

            if (sdFile.exists()) {
                sdFile.delete();
            }
        } catch (final Exception e) {
        }

    }

    public static long getDirSize(File dir) {
        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if (fileList[i].isDirectory()) {
                    result += getDirSize(fileList[i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                }
            }
            return result; // return the file size
        }
        return 0;
    }

    public static ArrayList<String> getDirAllFilesNames(final String dirPath) {
        final ArrayList<String> item = new ArrayList<String>();
        final ArrayList<String> path = new ArrayList<String>();

        final File f = new File(dirPath);
        final File[] files = f.listFiles();

        for (int i = 0; i < files.length; i++) {
            final File file = files[i];
            path.add(file.getPath());
            if (file.isDirectory()) {
                item.add(file.getName() + "/");
            } else {
                item.add(file.getName());
            }
        }
        return item;
    }

    public static File getExternalStorage() {
        return Environment.getExternalStorageDirectory();
    }

    public static boolean isESAvailable() {

        final String state = Environment.getExternalStorageState();
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

        if ((mExternalStorageAvailable == true)
                && (mExternalStorageWriteable == true)) {
            return true;
        } else {
            return false;
        }
    }

    public static Boolean isFileExist(final String Dir_On_SD,
                                      final String File_Name) {
        Boolean Exist = false;
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Exist = false;
        }
        try {
            File sdPath = Environment.getExternalStorageDirectory();
            sdPath = new File(sdPath.getAbsolutePath() + "/" + Dir_On_SD);
            final File sdFile = new File(sdPath, File_Name);

            if (sdFile.exists()) {
                Exist = true;
            }
        } catch (final Exception e) {
        }
        return Exist;

    }

    public static void mDir(final String dir) {
        if (isESAvailable()) {
            try {
                final File sdcard = Environment.getExternalStorageDirectory();
                final File newdir = new File(sdcard, dir);
                newdir.mkdirs();

            } catch (final Exception e) {
            }
        }
    }

    public static void writeToFile(final byte[] array, final String AppFolder,
                                   final String Folder, final String File_Name) {
        try {
            final String path = getExternalStorage() + Folder + "/" + File_Name;
            mDir(AppFolder);
            mDir(Folder);

            @SuppressWarnings("resource")
            final FileOutputStream stream = new FileOutputStream(path);
            try {
                stream.write(array);
            } catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (final FileNotFoundException e) {
            e.printStackTrace();

        }
    }

    public String readStringFromSd(final String Dir_On_SD,
                                   final String File_Name) {
        String File_Content = null;
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return null;
        }
        File sdPath = Environment.getExternalStorageDirectory();

        sdPath = new File(sdPath.getAbsolutePath() + "/" + Dir_On_SD);
        final File sdFile = new File(sdPath, File_Name);
        try {
            final BufferedReader br = new BufferedReader(new FileReader(sdFile));
            String str = "";
            while ((str = br.readLine()) != null) {
                File_Content += str;
            }
        } catch (final FileNotFoundException e) {
            return null;
        } catch (final IOException e) {
            return null;
        }
        return File_Content;
    }

    public String writeStringToSd(final String Dir_On_SD,
                                  final String File_Name, final String File_Content) {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return null;
        }
        File sdPath = Environment.getExternalStorageDirectory();
        sdPath = new File(sdPath.getAbsolutePath() + "/" + Dir_On_SD);
        sdPath.mkdirs();
        final File sdFile = new File(sdPath, File_Name);
        try {
            final BufferedWriter bw = new BufferedWriter(new FileWriter(sdFile));
            bw.write(File_Content);
            bw.close();
        } catch (final IOException e) {
            return null;
        }
        return sdFile.getAbsolutePath();
    }

    private String readFile(final Activity activity, final String File_Name) {
        String File_Content = null;
        try {
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    activity.openFileInput(File_Name)));
            String str = "";
            while ((str = br.readLine()) != null) {
                File_Content += str;
            }
        } catch (final FileNotFoundException e) {
            return null;
        } catch (final IOException e) {
            return null;
        }
        return File_Content;
    }

    // MODE_PRIVATE
    // MODE_WORLD_READABLE
    // MODE_WORLD_WRITEABLE
    // MODE_APPEND
    private Boolean writeFile(final Activity activity,
                              final String File_String, final String File_Name, final int MODE) {
        try {
            final BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(activity.openFileOutput(File_Name,
                            MODE))
            );
            bw.write(File_String);
            bw.close();
            return true;
        } catch (final FileNotFoundException e) {
            return false;
        } catch (final IOException e) {
            return false;
        }

    }
}
