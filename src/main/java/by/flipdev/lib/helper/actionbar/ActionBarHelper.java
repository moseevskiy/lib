package by.flipdev.lib.helper.actionbar;

import android.content.Context;
import android.util.TypedValue;

/**
 * Created by egormoseevskiy on 21.10.14.
 */
public class ActionBarHelper {

    public static int getActionBarSize(Context context) {
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return 0;
    }

//    public static void setBackIcon(View contentView,int backIcon) {
//        ViewGroup home = (ViewGroup) contentView.findViewById(android.R.id.home).getParent();
//        // get the first child (up imageview)
//        ( (ImageView) home.getChildAt(0) )
//                // change the icon according to your needs
//                .setImageResource(backIcon);
//    }

}
