package by.flipdev.lib.helper.convertation;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Window;

public class DP {
    public DisplayMetrics displayMetrics;
    public int STATUSBAR_HEIGHT;
    public int TITLEBAR_HEIGHT;
    public int SCREEN_WIDTH;
    public int SCREEN_HEIGHT;
    private Context context;

    public DP(Context context) {
        this.context = context;
        initConstants();
    }

    public static int get(Context context, float Dips) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                Dips, context.getResources().getDisplayMetrics());
    }

    public static DisplayMetrics getMetrix(Context context) {
        return context.getResources().getDisplayMetrics();
    }

    private void initConstants() {
        displayMetrics = context.getResources().getDisplayMetrics();
        Rect rectgle = new Rect();
        Window window = ((Activity) context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
        STATUSBAR_HEIGHT = rectgle.top;
        int contentViewTop =
                window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        TITLEBAR_HEIGHT = contentViewTop - STATUSBAR_HEIGHT;
        SCREEN_WIDTH = displayMetrics.widthPixels;
        SCREEN_HEIGHT = displayMetrics.heightPixels;
    }

    public int get(float Dips) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                Dips, context.getResources().getDisplayMetrics());
    }

    public DisplayMetrics getMetrix() {
        return context.getResources().getDisplayMetrics();
    }

}