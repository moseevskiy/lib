package by.flipdev.lib.helper.convertation;

import android.content.Context;
import android.view.View;

public class ResizeHelper {

    public static int[] fitToSize(int originalWidth, int originalHeight, int toWidth, int toHeight) {
        int[] newSize = new int[2];
        //Width/Height ratio of your image
        float imageOriginalWidthHeightRatio = (float) originalWidth / (float) originalHeight;

        //Calculate the new width and height of the image to display

        newSize[1] = toHeight;
        newSize[0] = (int) (imageOriginalWidthHeightRatio * newSize[1]);

        //Adjust the image width and height if bigger than screen
        if (newSize[0] > toWidth) {
            newSize[0] = toWidth;
            newSize[1] = (int) (newSize[0] / imageOriginalWidthHeightRatio);
        }
        return newSize;
    }

    public static int[] fitToWidth(int originalWidth, int originalHeight, int toWidth) {
        int[] newSize = new int[2];
        newSize[1] = toWidth * originalHeight / originalWidth;
        newSize[0] = newSize[1] * originalWidth / originalHeight;
        return newSize;
    }

    public static int[] fitToDisplay(Context context, int originalWidth, int originalHeight) {
        int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        int screenHeigth = context.getResources().getDisplayMetrics().heightPixels;
        return fitToSize(originalWidth, originalHeight, View.MeasureSpec.getSize(screenWidth), View.MeasureSpec.getSize(screenHeigth));
    }

    public static int[] fitToDisplayWidth(Context context, int originalWidth, int originalHeight) {
        int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        int screenHeigth = context.getResources().getDisplayMetrics().heightPixels;
        return fitToWidth(originalWidth, originalHeight, View.MeasureSpec.getSize(screenWidth));
    }
}
