package by.flipdev.lib.helper;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by egormosevskiy on 19.01.14.
 */
public class Time {

    public static Long getTime(String dateStr) {
        String pattern = "HH:mm";
        try {
            Date date = new SimpleDateFormat(pattern).parse(dateStr);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Long getTimeFromStrDate(String dateStr) {
        String pattern = "yyyy-MM-dd";
        try {
            Date date = new SimpleDateFormat(pattern).parse(dateStr);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTodaysDate() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        // fmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return fmt.format(timestamp);
    }

    public static String getDate(long time) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        // fmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return fmt.format(time);
    }

    public static String getTodaysTimeShtamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
        // fmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return fmt.format(timestamp);
    }

//    public static long getTodaysTime() {
//        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//        SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
//        String time = fmt.format(timestamp);
//        return DateHelper.parseTime(time).getTime();
//    }

}
