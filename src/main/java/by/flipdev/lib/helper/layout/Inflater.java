package by.flipdev.lib.helper.layout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by moseevskiy on 02.10.13.
 */
public class Inflater {
    Context context;
    LayoutInflater li;

    private Inflater(Context context) {
        this.context = context;
        li = LayoutInflater.from(context);//((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
    }

    public static Inflater create(Context context) {
        return new Inflater(context);
    }

    public static View inflate(Context context, int layout) {
        return ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(layout, null);
    }

    public View inflate(int layout) {
        return li.inflate(layout, null);
    }

}
