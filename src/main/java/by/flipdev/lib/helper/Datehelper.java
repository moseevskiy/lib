package by.flipdev.lib.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Egor Moseevskiy on 11.09.13.
 */
public class Datehelper {
    //2013-09-03T09:22:37Z
    public static java.util.Date parseDateTime(String dateString) {
        if (dateString == null) return null;
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

        try {
            return fmt.parse(dateString);
        } catch (java.text.ParseException e) {
            return null;
        }

    }

    public static java.util.Date parseTime(String time) {
        if (time == null) return null;
        DateFormat fmt = new SimpleDateFormat("HH:mm");

        try {
            return fmt.parse(time);
        } catch (java.text.ParseException e) {
            return null;
        }

    }
}
