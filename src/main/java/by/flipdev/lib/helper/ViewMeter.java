package by.flipdev.lib.helper;

import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;

import java.util.EventListener;

/**
 * Created by egormoseevskiy on 15.04.14.
 */
public class ViewMeter {
    public static void measure(final View view, final ViewMeterListener viewMeterListener) {
        if (view.getWidth() == 0 || view.getHeight() == 0) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    viewMeterListener.result(view.getWidth(), view.getHeight());
                    if (Build.VERSION.SDK_INT < 16) {
                        view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            });
        } else {
            viewMeterListener.result(view.getWidth(), view.getHeight());
        }
    }

    public interface ViewMeterListener extends EventListener {
        void result(int width, int height);
    }
}
