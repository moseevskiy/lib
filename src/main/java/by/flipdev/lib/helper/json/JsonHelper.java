package by.flipdev.lib.helper.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;

/**
 * Created by Flip on 07.12.13.
 */
public class JsonHelper {

    public static JSONObject readFromFile(String patch) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(patch)));
            JSONObject jsonObject = new JSONObject((String) in.readObject());
            in.close();
            return jsonObject;
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveToFile(JSONObject jsonObject, String patch) {
        try {
            File file = new File(patch);
            file.delete();
            ObjectOutput out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(jsonObject.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
