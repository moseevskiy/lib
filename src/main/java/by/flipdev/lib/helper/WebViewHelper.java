package by.flipdev.lib.helper;

/**
 * Created by egormoseevskiy on 22.04.14.
 */
public class WebViewHelper {

    public static String getHtmlTextWithFont(String htmlText, String font) {
        return "<html>\n" +
                "<head>\n" +
                "<title>Help page for this application</title>\n" +
                "<style type=\"text/css\">\n" +
                "body {\n" +
                "    background-color: #ebedf0;\n" +
                "}\n" +
                " \n" +
                "@font-face {\n" +
                "    font-family: roboto;\n" +
                "    src: url('file:///android_asset/fonts/" + font + "');\n" +
                "}\n" +
                " \n" +
                "p {\n" +
                "    color: #000000;\n" +
                "    font-family: roboto, Verdana, sans-serif;\n" +
                "}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <p style=\"font-family: roboto\">" + htmlText + "</p>\n" +
                " \n" +
                "</body>\n" +
                "</html>";
    }

}
