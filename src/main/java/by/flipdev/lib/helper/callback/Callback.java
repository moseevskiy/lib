package by.flipdev.lib.helper.callback;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

public class Callback {
    Context ctx;

    ProgressDialog spinner = null;

    public Callback(Context ctx) {
        this.ctx = ctx;
    }

    public static ProgressDialog spinner(final Context context,
                                         final String text) {
        final ProgressDialog spinner;
        spinner = new ProgressDialog(context);
        spinner.setMessage(text);
        spinner.setCancelable(false);
        spinner.show();
        return spinner;
    }

    public static void stopSpinner(final ProgressDialog spinner) {
        spinner.dismiss();
    }

    public static void toast(final Context context, final String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void toastLong(final Context context, final String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public Callback spinner(String text) {
        spinner = spinner(ctx, text);
        return this;
    }

    public Callback stopSpinner() {
        if (spinner != null) {
            spinner.dismiss();
            spinner = null;
        }
        return this;
    }

    public Callback toast(final String text) {
        Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
        return this;
    }

    public Callback toastLong(final String text) {
        Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
        return this;
    }

}
