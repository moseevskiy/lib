package by.flipdev.lib.helper.fonts;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

/**
 * Created by egormoseevskiy on 10.10.14.
 */
public class FontsHelper {
    private FontsHelper(Activity activity, FontsHelperInterface fontsHelperInterface) {
        setTypefaceToAll(activity, fontsHelperInterface);
    }

    public static View getActionBarView(Activity activity) {
        Window window = activity.getWindow();
        View v = window.getDecorView();
        int resId = activity.getResources().getIdentifier("action_bar_container", "id", "android");
        return v.findViewById(resId);
    }

    public static FontsHelper run(Activity activity, FontsHelperInterface fontsHelperInterface) {
        return new FontsHelper(activity, fontsHelperInterface);
    }

    @SuppressLint("NewApi")
    public static void scanForActionBarTextView(Activity activity, SearchTextViewInterface searchTextViewInterface) {
        if (activity == null || activity.getActionBar() == null || searchTextViewInterface == null)
            return;
        ActionBar actionBar = activity.getActionBar();
        String oldTitleText = "";
        if (actionBar.getTitle() != null)
            oldTitleText = actionBar.getTitle().toString();

        actionBar.setTitle("        ");
        View view = activity.findViewById(android.R.id.content).getRootView();
        searchForTextViewWithTitle(view, "        ", searchTextViewInterface);
        actionBar.setTitle(oldTitleText);
    }

    @SuppressLint("NewApi")
    public static void scanForTextViewWithText(Activity activity, String searchText, SearchTextViewInterface searchTextViewInterface) {
        if (activity == null || searchText == null || searchTextViewInterface == null)
            return;
        View view = activity.findViewById(android.R.id.content).getRootView();
        searchForTextViewWithTitle(view, searchText, searchTextViewInterface);
    }

    @SuppressLint("NewApi")
    public static void setActionBarFont(Activity activity, final Typeface typeface) {
        if (activity == null || activity.getActionBar() == null)
            return;
        ActionBar actionBar = activity.getActionBar();
        String oldTitleText = "";
        if (actionBar.getTitle() != null)
            oldTitleText = actionBar.getTitle().toString();
        actionBar.setTitle("        ");
        View view = getActionBarView(activity);
        searchForTextViewWithTitle(view, "        ", new SearchTextViewInterface() {
            @Override
            public void found(TextView title) {
                title.setTypeface(typeface);
            }
        });
        actionBar.setTitle(oldTitleText);
    }

    private static void setTypefaceToAll(Activity activity, FontsHelperInterface fontsHelperInterface) {
        View view = activity.findViewById(android.R.id.content).getRootView();
        setTypefaceToAll(view, fontsHelperInterface);
    }

    private static void setTypefaceToAll(View view, FontsHelperInterface fontsHelperInterface) {
        if (view instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) view;
            int count = g.getChildCount();
            for (int i = 0; i < count; i++)
                setTypefaceToAll(g.getChildAt(i), fontsHelperInterface);
        } else if (view instanceof TextView) {
            TextView textView = (TextView) view;
            if (fontsHelperInterface != null)
                fontsHelperInterface.setFont(textView);
        }
    }

    private static void searchForTextViewWithTitle(View view, String searchText, SearchTextViewInterface searchTextViewInterface) {
        if (view instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) view;
            int count = g.getChildCount();
            for (int i = 0; i < count; i++)
                searchForTextViewWithTitle(g.getChildAt(i), searchText, searchTextViewInterface);
        } else if (view instanceof TextView) {
            TextView textView = (TextView) view;
            if (textView.getText().toString().equals(searchText))
                if (searchTextViewInterface != null)
                    searchTextViewInterface.found(textView);
        }
    }

    public interface SearchTextViewInterface {
        void found(TextView title);
    }


    public interface FontsHelperInterface {
        void setFont(TextView textView);
    }

}
