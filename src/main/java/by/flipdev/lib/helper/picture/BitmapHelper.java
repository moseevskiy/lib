package by.flipdev.lib.helper.picture;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;

public class BitmapHelper {
    public static Bitmap getBitmapFromResource(final Context context,
                                               final int resourceId) {
        return BitmapFactory
                .decodeResource(context.getResources(), resourceId);
    }

    public static Bitmap getBitmapFromResource(final Context context,
                                               final int Resource_Id, final int Corner_Value) {
        return getRoundedCornerBitmap(BitmapFactory.decodeResource(
                context.getResources(), Resource_Id), Corner_Value);
    }

    public static Bitmap getBitmapFromResource(final Context context,
                                               final int Resource_Id, final int Color, final int Corner_Dips,
                                               final int Border_Dips) {
        return getRoundedCornerBitmap(BitmapFactory.decodeResource(
                        context.getResources(), Resource_Id), Color, Corner_Dips,
                Border_Dips, context
        );
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Bitmap getRoundedCornerBitmap(final Bitmap bitmap,
                                                final int roundPixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
                Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(),
                bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = roundPixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap getRoundedCornerBitmap(final Bitmap bitmap,
                                                final int color, final int cornerDips, final int borderDips,
                                                final Context context) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int borderSizePx = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, borderDips, context.getResources()
                        .getDisplayMetrics()
        );
        final int cornerSizePx = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, cornerDips, context.getResources()
                        .getDisplayMetrics()
        );
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        // prepare canvas for transfer
        paint.setAntiAlias(true);
        paint.setColor(0xFFFFFFFF);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

        // draw bitmap
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        // draw border
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderSizePx);
        canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

        return output;
    }

    public static Bitmap getCircleBitmapWithCorner(final Bitmap bitmap,
                                                   final int color, final int borderDips,
                                                   final Context context) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int borderSizePx = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, borderDips, context.getResources()
                        .getDisplayMetrics()
        );
        final int cornerSizePx = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 100, context.getResources()
                        .getDisplayMetrics()
        );
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        // prepare canvas for transfer
        paint.setAntiAlias(true);
        paint.setColor(0xFFFFFFFF);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

        // draw bitmap
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        // draw border
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderSizePx);
        canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

        return output;
    }

    public static Bitmap loadImageFromSD(final String dir,
                                         final String fileName) {
        return BitmapFactory.decodeFile(Environment
                .getExternalStorageDirectory().toString()
                + "/"
                + dir
                + "/"
                + fileName);
    }

    public static Bitmap loadImageFromSD(final String Patch) {
        return BitmapFactory.decodeFile(Environment
                .getExternalStorageDirectory().toString()
                + "/"
                + Patch);
    }

    public static String saveBitmapReplace(final Bitmap bitmap,
                                           final String dir, final String baseName) {
        try {
            final File sdcard = Environment.getExternalStorageDirectory();
            final File pictureDir = new File(sdcard, dir);
            pictureDir.mkdirs();
            File f = new File(pictureDir, baseName);
            try {
                f.delete();
            } catch (final Exception e) {
                e.printStackTrace();
            }
            final String name = f.getAbsolutePath();
            final FileOutputStream fos = new FileOutputStream(name);
            bitmap.compress(CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();

            return name;
            // }
        } catch (final Exception e) {
        }
        return null;
    }


    public static Bitmap getRoundedBitmap(Bitmap bitmap) {
        // Bitmap myCoolBitmap = ... ; // <-- Your bitmap you
        // want rounded
        int w = bitmap.getWidth(), h = bitmap.getHeight();

        // We have to make sure our rounded corners have an
        // alpha channel in most cases
        Bitmap rounder = Bitmap.createBitmap(w, h, Config.ARGB_8888);
        Canvas canvas = new Canvas(rounder);

        // We're going to apply this paint eventually using a
        // porter-duff xfer mode.
        // This will allow us to only overwrite certain pixels.
        // RED is arbitrary. This
        // could be any color that was fully opaque (alpha =
        // 255)
        Paint xferPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        xferPaint.setColor(Color.RED);

        // We're just reusing xferPaint to paint a normal
        // looking rounded box, the 20.f
        // is the amount we're rounding by.
        canvas.drawRoundRect(new RectF(0, 0, w, h), 5.0f, 5.0f, xferPaint);

        // Now we apply the 'magic sauce' to the paint
        xferPaint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));

        Bitmap result = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        Canvas resultCanvas = new Canvas(result);
        resultCanvas.drawBitmap(bitmap, 0, 0, null);
        resultCanvas.drawBitmap(rounder, 0, 0, xferPaint);

        return result;
    }

    public static void cutImage(ImageView imageView, Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
        Bitmap mutableBitmap = getViewBitmap(imageView).copy(Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);
        imageView.draw(canvas);
        Paint p = new Paint();
        p.setColor(Color.parseColor("#00000000"));
        for (int i = 0; i < imageView.getHeight() / 5; i++)
            canvas.drawLine(20, imageView.getHeight(), imageView.getWidth(), imageView.getHeight() - i, p);
        imageView.setImageBitmap(mutableBitmap);
    }

    public static Bitmap getViewBitmap(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.parseColor("#00000000"));
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public static Bitmap getGoneViewBitmap(View view) {
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.buildDrawingCache(true);
        Bitmap viewBitmap = Bitmap.createBitmap(view.getDrawingCache());

        view.setDrawingCacheEnabled(false);
        return viewBitmap;
    }

}
