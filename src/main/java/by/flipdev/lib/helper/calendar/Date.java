package by.flipdev.lib.helper.calendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Date {
    public static String backDate(final String Date, final String separator) {
        final String[] datearr = Date.split(separator);
        final int month = Integer.valueOf(datearr[1]) - 1;
        return datearr[0] + separator.replace("\\", "")
                + Integer.toString(month) + separator.replace("\\", "")
                + datearr[2];
    }

    public static int getDayOfWeek(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        int dow = c.get(Calendar.DAY_OF_WEEK);
        return dow;
    }

    public static String decDay(final String Date) {
        final String[] datearr = Date.split("\\.");
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(datearr[2]));
        calendar.set(Calendar.MONTH, Integer.parseInt(datearr[1]));
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datearr[0]) - 1);
        return Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ""
                + Integer.toString(calendar.get(Calendar.MONTH)) + "."
                + Integer.toString(calendar.get(Calendar.YEAR));
    }

    public static String fixDate(final String Date, final String separator) {
        final String[] datearr = Date.split(separator);
        final int month = Integer.valueOf(datearr[1]) + 1;
        return datearr[0] + separator.replace("\\", "")
                + Integer.toString(month) + separator.replace("\\", "")
                + datearr[2];
    }

    public static String incDay(final String Date) {
        final String[] datearr = Date.split("\\.");
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(datearr[2]));
        calendar.set(Calendar.MONTH, Integer.parseInt(datearr[1]));
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datearr[0]) + 1);
        return Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ""
                + Integer.toString(calendar.get(Calendar.MONTH)) + "."
                + Integer.toString(calendar.get(Calendar.YEAR));
    }

    public static Boolean isLaterDate(final String first_date,
                                      final String second_date, final String separator) {
        Boolean result = false;
        try {

            final String[] firstdatearr = first_date.split(separator);
            final String[] seconddatearr = second_date.split(separator);
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, Integer.parseInt(firstdatearr[2]));
            calendar1.set(Calendar.MONTH, Integer.parseInt(firstdatearr[1]));
            calendar1.set(Calendar.DAY_OF_MONTH,
                    Integer.parseInt(firstdatearr[0]));

            final Calendar calendar2 = Calendar.getInstance();
            calendar2.set(Calendar.YEAR, Integer.parseInt(seconddatearr[2]));
            calendar2.set(Calendar.MONTH, Integer.parseInt(seconddatearr[1]));
            calendar2.set(Calendar.DAY_OF_MONTH,
                    Integer.parseInt(seconddatearr[0]));

            long mills1 = calendar1.getTimeInMillis();
            long mills2 = calendar2.getTimeInMillis();

            if (mills1 > mills2) {
                result = true;
            }

        } catch (Exception e) {
        }
        return result;
    }

    public static Boolean isLaterTime(final String first_time,
                                      final String second_time, final String separator) {
        Boolean result = false;
        final String[] firsttimearr = first_time.split(separator);
        final String[] secondtimearr = second_time.split(separator);
        if (Integer.valueOf(firsttimearr[0]) > Integer
                .valueOf(secondtimearr[0])) {
            result = true;
        }
        if (Integer.valueOf(firsttimearr[0]) == Integer
                .valueOf(secondtimearr[0])) {
            if (Integer.valueOf(firsttimearr[1]) > Integer
                    .valueOf(secondtimearr[1])) {
                result = true;
            }
        }

        return result;
    }

    public static java.util.Date parseDateTime(String dateString) {
        if (dateString == null) return null;
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        try {
            return fmt.parse(dateString);
        } catch (java.text.ParseException e) {
            return null;
        }

    }
}
