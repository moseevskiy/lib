package by.flipdev.lib.helper.calendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Egor Moseevskiy on 11.09.13.
 */
public class DateHelper {
    //2013-09-03T09:22:37Z
    public static java.util.Date parseDateTime(String dateString) {
        if (dateString == null) return null;
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        try {
            return fmt.parse(dateString);
        } catch (java.text.ParseException e) {
            return null;
        }

    }
}
