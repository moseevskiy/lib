package by.flipdev.lib.helper.animation;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

public class AnimFactory {
    public static Animation inFromBottom(int duration) {
        Animation anim = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.0f);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    public static Animation fadeOut(int duration) {
        Animation anim = new AlphaAnimation(1, 0);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    public static Animation fadeIn(int duration) {
        Animation anim = new AlphaAnimation(0, 1);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }


    public static Animation inFromLeft(int duration) {
        Animation anim = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, -1.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    public static Animation inFromRight(int duration) {
        Animation anim = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    public static Animation inFromTop(int duration) {
        Animation anim = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, -1.0f,
                Animation.RELATIVE_TO_SELF, 0.0f);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    public static Animation outToBottom(int duration) {
        Animation anim = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 1.0f);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    public static Animation outToLeft(int duration) {
        Animation anim = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    public static Animation outToRight(int duration) {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(duration);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    public static Animation outToTop(int duration) {
        Animation anim = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f);
        anim.setDuration(duration);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }


    public static Animation blink(int duration) {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(duration); //You can manage the time of the blink with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.RESTART);
        anim.setRepeatCount(0);
        return anim;
    }

}