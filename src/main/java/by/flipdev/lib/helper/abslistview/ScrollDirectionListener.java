package by.flipdev.lib.helper.abslistview;

public interface ScrollDirectionListener {
    void onScrollDown();

    void onScrollUp();
}