package by.flipdev.lib.helper.string;

/**
 * User: Egor Moseevskiy
 * Date: 13.10.13
 * Time: 3:08
 * To change this template use File | ServerSettings | File Templates.
 */
public class StringValidator {
    public static boolean isContainsOnlyLetters(String str) {
        char[] chars = str.toCharArray();
        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isContainsOnlyLatinLetters(String str) {
        return str.matches("[a-zA-Z]+");
    }

    public static boolean isContainsOnlyLatinLettersAndNumbers(String str) {
        return str.matches("[a-zA-Z0-9]+");
    }

    public static boolean isContainsOnlyNumbers(String str) {
        return str.matches("[0-9]+");
    }

    public static boolean isNumber(String str) {
        return str.matches("[0-9]+");
    }
}
