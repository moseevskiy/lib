package by.flipdev.lib.url;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import by.flipdev.lib.task.Task;
import by.flipdev.lib.task.listeners.AsyncInterface;
import by.flipdev.lib.url.ssl.VerifyAllSSLSocketFactory;

public class RestClient {
    private HttpClient httpClient;
    private String url;

    private RestClient(HttpClient httpClient, String url, int timeout) {
        this.httpClient = httpClient;
        this.url = url;

        if (httpClient == null)
            this.httpClient = getClient(timeout);
    }

    public static HttpResponse get(String url, int timeout) throws IOException {
        HttpGet httpGetRequest = new HttpGet(url);
        return getClient(timeout).execute(httpGetRequest);
    }

    public static String getString(String url, int timeout) throws IOException {
        AbstractHttpClient client = getClient(timeout);
        HttpGet httpGetRequest = new HttpGet(url);
        HttpResponse httpResponse = client.execute(httpGetRequest);
        return makeString(httpResponse.getEntity().getContent());
    }

    public static JSONObject getJson(String url, int timeout) throws IOException, JSONException {
        String resp = getString(url, timeout);
        if (resp == null)
            return null;

        return new JSONObject(resp);

    }

    public static JSONObject post(String url, int timeout) {
        try {
            AbstractHttpClient client = getClient(timeout);
            HttpPost httpPostRequest = new HttpPost(url);
            HttpResponse httpResponse = client.execute(httpPostRequest);
            return new JSONObject(makeString(httpResponse.getEntity().getContent()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static String makeString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuilder builder = new StringBuilder();
        String aux = "";
        while ((aux = reader.readLine()) != null) {
            builder.append(aux);
        }
        return builder.toString();
    }

    public static HttpResponse post(String url, String data) {
        try {
            AbstractHttpClient httpclient = getClient(5000);
            HttpPost httpPostRequest = new HttpPost(url);
            StringEntity se;
            se = new StringEntity(data);
            // Set HTTP parameters
            httpPostRequest.setEntity(se);
            httpPostRequest.setHeader("Accept", "application/x-www-form-urlencoded");
            httpPostRequest.setHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPostRequest.setHeader("Accept-Encoding", "gzip"); // only set this parameter if you would like to use gzip compression
            return httpclient.execute(httpPostRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static AbstractHttpClient getClient(final int timeout) {
        final AbstractHttpClient client = new DefaultHttpClient() {
            @Override
            protected ClientConnectionManager createClientConnectionManager() {
                SchemeRegistry registry = new SchemeRegistry();
                registry.register(
                        new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
                registry.register(
                        new Scheme("https", VerifyAllSSLSocketFactory.get(), 443));
                HttpParams params = getParams();
                HttpConnectionParams.setConnectionTimeout(params, timeout);
                HttpConnectionParams.setSoTimeout(params, timeout);
//                HttpProtocolParams.setUserAgent(params, client.getUserAgent(HttpProtocolParams.getUserAgent(params)));
                return new ThreadSafeClientConnManager(params, registry);
            }
        };
        return client;
    }

    private Task getAsync(final GetAsyncListener listener) {
        return Task.async(new AsyncInterface() {

            @Override
            public Object asyncWorkWithResult() {
                HttpResponse response = null;
                try {
                    response = get();
                } catch (IOException e) {
                    e.printStackTrace();
                    return "CONNECTION_ERROR";
                }
                if (listener != null)
                    return listener.makeWhatIsYouNeadAsync(response);
                return null;
            }

            @Override
            public void afterAsync(Object result) {
                try {
                    if (result.equals("CONNECTION_ERROR"))
                        if (listener != null)
                            listener.failConnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (listener != null)
                    listener.complete(result);
            }
        });
    }

    public void getString(GetStringListener getStringListener) {
        HttpGet httpGetRequest = new HttpGet(url);
        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(httpGetRequest);
        } catch (IOException e) {
            e.printStackTrace();
            if (getStringListener != null)
                getStringListener.failConnect();
            return;
        }
        try {
            if (getStringListener != null)
                getStringListener.complete(makeString(httpResponse.getEntity().getContent()));
        } catch (IOException e) {
            e.printStackTrace();
            if (getStringListener != null)
                getStringListener.failIsNotAString();
        }
    }

    public HttpResponse get() throws IOException {
        HttpGet httpGetRequest = new HttpGet(url);
        return httpClient.execute(httpGetRequest);
    }

    public interface GetAsyncListener {
        Object makeWhatIsYouNeadAsync(HttpResponse httpResponse);

        void failConnect();

        void complete(Object result);
    }


    public interface GetStringListener {
        void failIsNotAString();

        void failConnect();

        void complete(String result);
    }


}
