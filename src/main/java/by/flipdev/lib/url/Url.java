package by.flipdev.lib.url;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Url {
    public static final String SLASH = "/";
    private static final int DEF_TIMEOUT = 10000;
    private static final String HTTP = "http://";
    private static final String HTTPS = "https://";
    private String QUESTION = "?";

    private boolean https;

    private String url = "";
    private String method = "";
    private String params = "";

    private Url(String url, String method, boolean https) {
        this.url = url;
        this.method = method;
        this.https = https;
    }

    public static Url create(String url, String method, boolean https) {
        return new Url(url, method, https);
    }

    public static Url create(String url, String method) {
        return new Url(url, method, false);
    }

    public static Url create(String url, boolean https) {
        return new Url(url, "", https);
    }

    public static Url create(String url) {
        return new Url(url, "", false);
    }

    public static Url create() {
        return new Url("", "", false);
    }

    public Url setMethod(String method) {
        this.method = method;
        return this;
    }

    public Url setHttps(boolean https) {
        this.https = https;
        return this;
    }

    public Url setUrl(String url) {
        this.url = url;
        return this;
    }

    public Url addParam(String param, String value) {
        if (params.length() != 0)
            params += "&";
        params += param + "=" + value;
        return this;
    }

    public Url addMethod(String method) {
        if (this.method.length() != 0)
            this.method += SLASH + method;
        return this;
    }

    public Url add(String param, Integer value) {
        return addParam(param, String.valueOf(value));
    }

    public String make() {

        if (method.length() > 0) {
            if (!url.substring(url.length() - 1, url.length()).equals(SLASH))
                url += SLASH;
            return fix(url + method + QUESTION + params);
        } else {
            return fix(url);
        }
    }

    public String get() throws IOException {
        return RestClient.getString(make(), DEF_TIMEOUT);
    }

    public String get(int timeout) throws IOException {
        return RestClient.getString(make(), timeout);
    }

    public JSONObject getJson() throws IOException, JSONException {
        return RestClient.getJson(make(), DEF_TIMEOUT);
    }

    public JSONObject getJson(int timeout) throws JSONException, IOException {
        return RestClient.getJson(make(), timeout);
    }

    public void clearParams() {
        params = "";
    }


    private String fix(String url) {
        if (!url.contains(HTTP) && !url.contains(HTTPS)) {
            String tempUrl = url;
            url = "";
            if (https)
                url += HTTPS + tempUrl;
            else
                url += HTTP + tempUrl;
        } else {
            if (url.contains(HTTP) && https) {
                url = url.replace(HTTP, HTTPS);
            }
            if (url.contains(HTTPS) && !https) {
                url = url.replace(HTTPS, HTTP);
            }
        }
        return url;
    }

    public Url setUrlParam(String param, String value) {
        url = replaceParam(url, param, value);
        return this;
    }

    public Url setUrlParam(String param, Integer value) {
        setUrlParam(param, String.valueOf(value));
        return this;
    }

    public Url setMetodParam(String param, String value) {
        method = replaceParam(method, param, value);
        return this;
    }

    public Url setMetodParam(String param, Integer value) {
        setMetodParam(param, String.valueOf(value));
        return this;
    }

    private String replaceParam(String str, String param, String value) {
        String completeParam = "{" + param + "}";
        if (!str.contains(completeParam))
            throw new ParamNotExistsException(completeParam, str);
        str = str.replace(completeParam, value);
        return str;
    }

    public class ParamNotExistsException extends RuntimeException {
        public ParamNotExistsException(String param, String formattingString) {
            super("Couldn't find param \"" + param + "\" in string \"" + formattingString + "\".");
        }
    }
}