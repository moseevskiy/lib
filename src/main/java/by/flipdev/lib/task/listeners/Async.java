package by.flipdev.lib.task.listeners;


public interface Async {
    void before();

    void asyncWork();

    Object asyncWorkWithResult();

    void afterAsync(Object result);

    void afterAsync();

    void afterDelay();
}

