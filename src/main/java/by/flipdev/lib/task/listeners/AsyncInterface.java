package by.flipdev.lib.task.listeners;


public abstract class AsyncInterface implements Async {
    @Override
    public void before() {

    }

    @Override
    public Object asyncWorkWithResult() {
        return null;
    }

    @Override
    public void asyncWork() {

    }

    @Override
    public void afterAsync() {

    }

    @Override
    public void afterAsync(Object result) {

    }

    @Override
    public void afterDelay() {

    }
}



