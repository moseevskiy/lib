/*******************************************************************************
 * Copyright 2013 Egor Moseevskiy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package by.flipdev.lib.task;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;

import by.flipdev.lib.helper.Delay;
import by.flipdev.lib.task.listeners.AsyncInterface;


public class Task extends AsyncTask<Void, Void, Object> {

    private final boolean onlyDelay;
    private AsyncInterface asyncInterface = null;

    private int delay = 0;

    private boolean run;
    private boolean inProgress = false;
    private Runnable delayRunnable = new Runnable() {
        @Override
        public void run() {
            if (asyncInterface != null)
                asyncInterface.afterDelay();
            if (run)
                Task.this.runAsync();
            else
                inProgress = false;
        }
    };
    private Handler handler = new Handler();

    private Task(AsyncInterface asyncInterface, int delay, boolean onlyDelay) {
        this.asyncInterface = asyncInterface;
        this.delay = delay;
        this.onlyDelay = onlyDelay;

        run();
    }

    public static Task async(AsyncInterface asyncInterface) {
        return async(asyncInterface, 0);
    }

    public static Task delay(AsyncInterface asyncInterface, int delay) {
        return new Task(asyncInterface, delay, true);
    }

    public static Task async(AsyncInterface asyncInterface, int delay) {
        return new Task(asyncInterface, delay, false);
    }

    public boolean run() {
        if (inProgress == false) {
            inProgress = true;
            if (asyncInterface != null)
                asyncInterface.before();

            if (delay == 0 && onlyDelay) {
                if (asyncInterface != null)
                    asyncInterface.afterDelay();
            }
            if (delay == 0 && !onlyDelay) {
                runAsync();
            }
            if (delay > 0 && onlyDelay) {
                run = false;
                handler.postDelayed(delayRunnable, delay);
            }
            if (delay > 0 && !onlyDelay) {
                run = true;
                handler.postDelayed(delayRunnable, delay);
            }
            return true;
        }
        return false;
    }

    private void runAsync() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            execute();
        }
    }

    @Override
    protected Object doInBackground(Void... voids) {
        if (delay > 0)
            Delay.thread(delay);
        if (asyncInterface != null) {
            asyncInterface.asyncWork();
            return asyncInterface.asyncWorkWithResult();
        }
        return null;
    }


    @Override
    protected void onPostExecute(Object result) {
        if (asyncInterface != null) {
            asyncInterface.afterAsync();
            asyncInterface.afterAsync(result);
        }
        inProgress = false;
    }

    public void stop() {
        handler.removeCallbacks(delayRunnable);
        cancel(false);
    }

}

/*
        Task.async(new AsyncInterface() {
            @Override
            public void before() {
                super.before();
            }

            @Override
            public void afterDelay() {
                super.afterDelay();
            }

            @Override
            public void asyncWork() {
                super.asyncWork();
            }

            @Override
            public Object asyncWorkWithResult() {
                return super.asyncWorkWithResult();
            }

            @Override
            public void afterAsync() {
                super.afterAsync();
            }

            @Override
            public void afterAsync(Object result) {
                super.afterAsync(result);
            }

        }, DELAY);


        Task.async(new AsyncInterface() {
            @Override
            public void before() {
                super.before();
            }

            @Override
            public void asyncWork() {
                super.asyncWork();
            }

            @Override
            public Object asyncWorkWithResult() {
                return super.asyncWorkWithResult();
            }

            @Override
            public void afterAsync() {
                super.afterAsync();
            }

            @Override
            public void afterAsync(Object result) {
                super.afterAsync(result);
            }

        });

        Task.async(new AsyncInterface() {
            @Override
            public void asyncWork() {
                super.asyncWork();
            }
        });

        Task.delay(new AsyncInterface() {
            @Override
            public void afterDelay() {
                super.afterDelay();
            }
        }, DELAY);
 */