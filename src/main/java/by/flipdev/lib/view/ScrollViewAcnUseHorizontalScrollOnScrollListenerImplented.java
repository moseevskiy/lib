package by.flipdev.lib.view;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by egormoseevskiy on 14.10.14.
 */
public class ScrollViewAcnUseHorizontalScrollOnScrollListenerImplented extends ScrollViewCanUseViewPager {

    private Listener listener;

    private int width, height;

    public ScrollViewAcnUseHorizontalScrollOnScrollListenerImplented(Context context) {
        super(context);
    }

    public ScrollViewAcnUseHorizontalScrollOnScrollListenerImplented(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollViewAcnUseHorizontalScrollOnScrollListenerImplented(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ScrollViewAcnUseHorizontalScrollOnScrollListenerImplented setListener(Listener Listener) {
        this.listener = Listener;
        return this;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (listener != null)
            listener.onScrollChanged(l, t, oldl, oldt);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);

        this.setMeasuredDimension(
                parentWidth, parentHeight);
        if (width != parentWidth || height != parentHeight) {
            width = parentWidth;
            height = parentHeight;
            if (listener != null) listener.sizeChanged(width, height);
        }
    }

    public interface Listener {
        void sizeChanged(int w, int h);

        void onScrollChanged(int l, int t, int oldl, int oldt);
    }


}
