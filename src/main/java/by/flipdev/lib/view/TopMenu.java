package by.flipdev.lib.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import by.flipdev.lib.helper.ViewMeter;


/**
 * Created by egormoseevskiy on 23.04.14.
 */
public class TopMenu extends LinearLayout {

    private static final int SPEED = 15;
    private static final int DELAY = 2;
    public boolean stateTop = true;
    Runnable smoothTopRunnable = new Runnable() {
        @Override
        public void run() {
            smoothTop();
        }
    };
    Runnable smoothBottomRunnable = new Runnable() {
        @Override
        public void run() {
            smoothBottom();
        }
    };
    private boolean canTouch = true;
    private LayoutParams contentParams;
    private LayoutParams topParams;
    private ContentSide content = null;
    private View top = null;
    private TopMenuListener listener;
    private Context context;
    private float dy = 0, y = 0;

    public TopMenu(Context context) {
        super(context);
        init();
    }

    public TopMenu(View content, View top) {
        super(content.getContext());
        context = getContext();
        setTop(top);
        setContent(content);
    }

    public TopMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public static TopMenu create(View content, View side) {
        return new TopMenu(content, side).top();
    }

    public void setCanTouch(boolean canTouch) {
        this.canTouch = canTouch;
    }

    public boolean getStateTop() {
        return stateTop;
    }

    public boolean tougle() {
        if (getStateTop()) {
            smoothBottom();
        } else {
            smoothTop();
        }
        return stateTop;
    }

    public TopMenu setTop(View top) {
        this.top = top;
        return this;
    }

    public void setListener(TopMenuListener listener) {
        this.listener = listener;
    }

    public TopMenu setContent(View content) {
        this.content = new ContentSide(content);
        return this;
    }

    public void init() {
        setOrientation(VERTICAL);
        ViewMeter.measure(this, new ViewMeter.ViewMeterListener() {
            @Override
            public void result(int width, int height) {
                View contentView = getChildAt(1);
                top = getChildAt(0);
                removeAllViews();
                content = new ContentSide(contentView);


                addView(top);
                addView(content);
                setParams(width, height);
                topParams = (LayoutParams) top.getLayoutParams();
                topParams.topMargin = -top.getHeight();
                top.setLayoutParams(topParams);
            }
        });
    }

    private void setParams(int width, int height) {
        contentParams = new LayoutParams(width, height);
        topParams = new LayoutParams(width, top.getHeight());
        topParams.topMargin = -top.getHeight();
        content.setLayoutParams(contentParams);
        top.setLayoutParams(topParams);
    }

    public void setMenuHeight(int height) {
        topParams = (LinearLayout.LayoutParams) top.getLayoutParams();
        topParams.height = height;
        top.setLayoutParams(topParams);
        topParams.topMargin = -height;
        top.setLayoutParams(topParams);
    }

    private int getPercent() {
        int size = top.getHeight();
        int pos = topParams.topMargin;
        return 100 + ((pos * 100) / size);
    }

    public TopMenu top() {
        try {
            topParams = (LayoutParams) top.getLayoutParams();
            topParams.topMargin = -top.getHeight();
            top.setLayoutParams(topParams);
            notifyTop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public TopMenu bottom() {
        try {
            topParams = (LayoutParams) top.getLayoutParams();
            topParams.topMargin = 0;
            top.setLayoutParams(topParams);
            notifyBottom();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public void smoothTop() {
        removeCallbacks(smoothTopRunnable);
        removeCallbacks(smoothBottomRunnable);
        if (topParams.topMargin - SPEED < -top.getHeight()) {
            top();
            stateTop = true;
            return;
        } else {
            topParams.topMargin -= SPEED;
            top.setLayoutParams(topParams);
            postDelayed(smoothTopRunnable, DELAY);
        }
    }

    public void smoothBottom() {
        removeCallbacks(smoothTopRunnable);
        removeCallbacks(smoothBottomRunnable);
        if (topParams.topMargin + SPEED > 0) {
            bottom();
            stateTop = false;
        } else {
            topParams.topMargin += SPEED;
            top.setLayoutParams(topParams);
            postDelayed(smoothBottomRunnable, DELAY);
        }

    }

    private void notifyTop() {
        if (!stateTop)
            if (listener != null)
                listener.top();
    }

    private void notifyBottom() {
        if (stateTop)
            if (listener != null)
                listener.bottom();
    }


    private void fixParams() {
        if (topParams.topMargin < -top.getHeight()) {
            topParams.topMargin = -top.getHeight();
        }
        if (topParams.topMargin > 0) {
            topParams.topMargin = 0;
        }
        top.setLayoutParams(topParams);


    }


    public interface TopMenuListener {
        void top();

        void bottom();
    }

    private class ContentSide extends FrameLayout {
        public ContentSide(View content) {
            super(content.getContext());
            addView(content);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            if (canTouch) {
                switch (event.getAction()) {


                    case MotionEvent.ACTION_DOWN: {

                        removeCallbacks(smoothTopRunnable);
                        removeCallbacks(smoothBottomRunnable);
                        contentParams = (LinearLayout.LayoutParams) getLayoutParams();
                        topParams = (LinearLayout.LayoutParams) top.getLayoutParams();

                        dy = event.getRawY() - topParams.topMargin;

                    }
                    break;

                    case MotionEvent.ACTION_MOVE: {
                        y = event.getRawY();
                        topParams.topMargin = (int) (y - dy);
                        fixParams();
                    }
                    break;

                    case MotionEvent.ACTION_UP: {
                        int percent = getPercent();
                        if (percent < 50) {
                            smoothTop();
                        }
                        if (percent >= 50) {
                            smoothBottom();
                        }


                    }
                    break;
                }

                return true;
            }
            return false;
        }


    }
}
