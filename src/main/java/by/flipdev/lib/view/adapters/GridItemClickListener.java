package by.flipdev.lib.view.adapters;

import android.view.View;

public interface GridItemClickListener {

    void onGridItemClicked(View v, int position, long itemId);

}