package by.flipdev.lib.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import by.flipdev.lib.helper.picture.BitmapHelper;

/**
 * Created by egormoseevskiy on 08.10.14.
 */
public class FreezableView extends FrameLayout {
    private Context context;
    private Bitmap viewCache;
    private int w, h;

    public FreezableView(Context context) {
        super(context);
        init(context);
    }

    public FreezableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FreezableView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
    }

//    @Override
//    protected void onDraw(Canvas canvas) {
//        if(canvas==null && canvas.getWidth() == 0 && canvas.getHeight() == 0)
//            return;
//        if(canvas.getWidth() != w || canvas.getHeight() != h){
//            w = canvas.getWidth();
//            h = canvas.getHeight();
//            super.onDraw(canvas);
//            viewCache = BitmapHelper.getViewBitmap(this);
//        }
//    }

    private void freeze() {
        viewCache = BitmapHelper.getViewBitmap(this);
        getChildAt(0).setVisibility(GONE);
        setBackgroundDrawable(new BitmapDrawable(viewCache));
    }

    private void unFreeze() {
        viewCache = null;
        getChildAt(0).setVisibility(VISIBLE);
        setBackgroundDrawable(null);
    }

}
