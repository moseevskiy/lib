//package by.flipdev.lib.view;
//
//import android.os.Bundle;
//import android.support.v7.app.ActionBarActivity;
//
//public class SlidingPaneActionBarActivity extends ActionBarActivity {
//    ImprovedSlidingPaneLayout improvedSlidingPaneLayout;
//    int layout;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        improvedSlidingPaneLayout = new ImprovedSlidingPaneLayout(this);
//        super.onCreate(savedInstanceState);
//    }
//
//    public void setContentView(int layout) {
//        this.layout = layout;
//        improvedSlidingPaneLayout.setContentView(getLayoutInflater().inflate(layout, null));
//        setContentView(improvedSlidingPaneLayout);
//    }
//
//    public void makeActionBarSlide() {
//        setContentView(getLayoutInflater().inflate(layout, null));
//        improvedSlidingPaneLayout.makeActionBarSlide(getWindow());
//
//    }
//
//    public ImprovedSlidingPaneLayout getImprovedSlidingPaneLayout() {
//        return improvedSlidingPaneLayout;
//    }
//
//
//}