//package by.flipdev.lib.view;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.Drawable;
//import android.graphics.drawable.GradientDrawable;
//import android.support.v4.view.ViewPager;
//import android.util.AttributeSet;
//import android.view.Gravity;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//
//import java.util.EventListener;
//
//import by.flipdev.lib.helper.convertation.DP;
//
//public class PagerIndicator extends LinearLayout {
//    Drawable background;
//    Context context;
//    ViewPager pager;
//    Drawable active;
//    Drawable inactive;
//
//    // default values for the indicator
//    int numPages;
//    int currentPage;
//    int activeRadius;
//    int inactiveRadius;
//    int colorActive = Color.WHITE;
//    int colorInactive = Color.GRAY;
//    PageChangeListener pageChangeListener = null;
//    DP dp;
//
//    public PagerIndicator(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        init(context);
//    }
//
//    public PagerIndicator(Context context) {
//        super(context);
//        init(context);
//    }
//
//    // Initializes layout
//    private void init(Context context) {
//        this.context = context;
//        dp = new DP(context);
//        setOrientation(HORIZONTAL);
//        setBackgroundDrawable(background);
//        activeRadius = dp.get(25);
//        inactiveRadius = dp.get(20);
//        active = circle(true, colorActive, activeRadius);
//        inactive = circle(true, colorInactive, inactiveRadius);
//
//    }
//
//    public void setPager(ViewPager pager) {
//        this.pager = pager;
//        draw();
//    }
//
//    public void setPageChangeListener(PageChangeListener listener) {
//        pageChangeListener = listener;
//    }
//
//    private GradientDrawable circle(boolean isActive, int color, int size) {
//        GradientDrawable circle = new GradientDrawable();
//        circle.setShape(GradientDrawable.OVAL);
//        if (isActive) {
//            circle.setSize(size, size);
//            circle.setColor(color);
//        } else {
//            circle.setSize(size, size);
//            circle.setColor(color);
//        }
//        return circle;
//    }
//
//    /*
//    *  Methods for setting background for indicator
//    */
//    public PagerIndicator setDrawableBackground(Drawable drawable) {
//        background = drawable;
//        return this;
//    }
//
//    public PagerIndicator setBitmapBackground(Bitmap bitmap) {
//        background = new BitmapDrawable(getResources(), bitmap);
//        return this;
//    }
//
//    /*
//    * Methods for changing active indicator
//    */
//    public PagerIndicator setColorActive(int color) {
//        colorActive = color;
//        return this;
//    }
//
//    public PagerIndicator setRadiusActive(int radius) {
//        activeRadius = radius;
//        return this;
//    }
//
//    public PagerIndicator setBitmapActive(Bitmap bitmap) {
//        active = new BitmapDrawable(getResources(), bitmap);
//        return this;
//    }
//
//
//    public PagerIndicator setDrawableActive(Drawable drawable) {
//        active = drawable;
//        return this;
//    }
//
//
//    /*
//    * Methods for changing inactive indicator
//    */
//    public PagerIndicator setColorInactive(int color) {
//        colorInactive = color;
//        return this;
//    }
//
//    public PagerIndicator setRadiusInactive(int radius) {
//        inactiveRadius = radius;
//        return this;
//    }
//
//    public PagerIndicator setBitmapInactive(Bitmap bitmap) {
//        inactive = new BitmapDrawable(getResources(), bitmap);
//        return this;
//    }
//
//    public PagerIndicator setDrawableInactive(Drawable drawable) {
//        inactive = drawable;
//        return this;
//    }
//
//    // Creates ImageViews with given drawables (shape or image) and sets active indicator on current page
//    private void draw() {
//        numPages = pager.getAdapter().getCount();
//        currentPage = pager.getCurrentItem();
//        for (int i = 0; i < numPages; i++) {
//            ImageView indicator = createImageView(i);
//            if (i == currentPage) {
//                indicator.setImageDrawable(active);
//            } else {
//                indicator.setImageDrawable(inactive);
//            }
//
//            addView(indicator);
//        }
//        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int i, float v, int i2) {
//
//            }
//
//            @Override
//            public void onPageSelected(int i) {
//                if (pageChangeListener != null)
//                    pageChangeListener.onPageSelected(i);
//                for (int j = 0; j < numPages; j++) {
//                    ImageView indicator = (ImageView) findViewById(j);
//                    if (j == i) {
//                        indicator.setImageDrawable(active);
//                    } else {
//                        indicator.setImageDrawable(inactive);
//                    }
//                }
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int i) {
//            }
//        });
//    }
//
//    private ImageView createImageView(int id) {
//        ImageView indicator = new ImageView(context);
//        indicator.setId(id);
//        indicator.setScaleType(ImageView.ScaleType.FIT_XY);
//        LayoutParams lp = null;
//        lp = new LayoutParams(activeRadius, activeRadius);
//        lp.gravity = Gravity.CENTER_VERTICAL;
//        lp.setMargins(5, 1, 5, 1);
//        indicator.setLayoutParams(lp);
//
//        return indicator;
//    }
//
//    public interface PageChangeListener extends EventListener {
//        void onPageSelected(int i);
//    }
//}
