package by.flipdev.lib.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

/**
 * Created by egormoseevskiy on 14.10.14.
 */
public class HorizontalScrollViewOnScrollListenerImplented extends HorizontalScrollView {

    private ScrolListener scrolListener;

    public HorizontalScrollViewOnScrollListenerImplented(Context context) {
        super(context);
    }

    public HorizontalScrollViewOnScrollListenerImplented(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalScrollViewOnScrollListenerImplented(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setScrolListener(ScrolListener scrolListener) {
        this.scrolListener = scrolListener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (scrolListener != null)
            scrolListener.onScrollChanged(l, t, oldl, oldt);
    }

    public interface ScrolListener {
        void onScrollChanged(int l, int t, int oldl, int oldt);
    }
}
