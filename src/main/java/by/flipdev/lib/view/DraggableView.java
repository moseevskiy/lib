package by.flipdev.lib.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.Random;

import by.flipdev.lib.helper.ViewMeter;

/**
 * Created by egormoseevskiy on 23.04.14.
 */
public class DraggableView extends FrameLayout {
    private static final int SPEED = 15;
    private static final int DELAY = 1;
    Runnable smoothTopRunnable = new Runnable() {
        @Override
        public void run() {
            smoothTop();
        }
    };
    Runnable smoothBottomRunnable = new Runnable() {
        @Override
        public void run() {
            smoothBottom();
        }
    };
    Runnable smoothLeftRunnable = new Runnable() {
        @Override
        public void run() {
            smoothLeft();
        }
    };
    Runnable smoothRightRunnable = new Runnable() {
        @Override
        public void run() {
            smoothRight();
        }
    };
    boolean click = true;
    private Context context;
    private LinearLayout.LayoutParams parms;
    private View view = null;
    private float dx = 0, dy = 0, x = 0, y = 0;
    private TopListener topListener;
    private BottomListener bottomListener;
    private LeftListener leftListener;
    private RightListener rightListener;
    private DragListener dragListener;
    private ClickListener clickListener;
    private View parent;
    private boolean canUse = false;
    private boolean dragVertical = true;
    private boolean dragHorizontal = true;
    private boolean canDragOutsideView = true;

    public DraggableView(Context context) {
        super(context);
        init();
    }

    public DraggableView(Context context, View view) {
        super(context);
        this.view = view;
        setMovableView(view);
        init();
    }

    public DraggableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DraggableView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public DraggableView setDragListener(DragListener dragListener) {
        this.dragListener = dragListener;
        return this;
    }

    public DraggableView setTopListener(TopListener topListener) {
        this.topListener = topListener;
        return this;
    }

    public DraggableView setBottomListener(BottomListener bottomListener) {
        this.bottomListener = bottomListener;
        return this;
    }

    public DraggableView setLeftListener(LeftListener leftListener) {
        this.leftListener = leftListener;
        return this;
    }

    public DraggableView setRightListener(RightListener rightListener) {
        this.rightListener = rightListener;
        return this;
    }

    public DraggableView setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
        return this;
    }

    public DraggableView setCanUse(boolean canUse) {
        this.canUse = canUse;
        return this;
    }

    public DraggableView setDragHorizontal(boolean dragHorizontal) {
        this.dragHorizontal = dragHorizontal;
        return this;
    }

    public DraggableView setDragVertical(boolean dragVertical) {
        this.dragVertical = dragVertical;
        return this;
    }

    public DraggableView setCanDragOutsideView(boolean canDragOutsideView) {
        this.canDragOutsideView = canDragOutsideView;
        return this;
    }

    public void setMovableView(View view) {
        removeAllViews();
        this.view = view;
        addView(view);
    }

    private void init() {
        context = getContext();
        ViewMeter.measure(this, new ViewMeter.ViewMeterListener() {
            @Override
            public void result(int width, int height) {
                parent = ((View) getParent());
                canUse = true;
            }
        });

    }

    public int getPercentVertical() {
        if (getSizeVertical() != 0)
            return ((getPositionVertical() * 100) / getSizeVertical());
        return 0;
    }

    public int getPercentHorizontal() {
        if (getSizenHorizontal() != 0)
            return ((getPositionHorizontal() * 100) / getSizenHorizontal());
        return 0;
    }

    public int getSizeVertical() {
        if (parent == null)
            parent = ((View) getParent());
        return parent.getHeight() - getHeight();
    }

    public int getSizenHorizontal() {
        if (parent == null)
            parent = ((View) getParent());
        return parent.getWidth() - getWidth();
    }

    public int getPositionVertical() {
        return parms.topMargin;
    }

    public int getPositionHorizontal() {
        return parms.leftMargin;
    }

    public DraggableView top() {
        try {
            parms = (LinearLayout.LayoutParams) getLayoutParams();
            parms.topMargin = 0;
            setLayoutParams(parms);
//            //canTouch = true;
            if (topListener != null)
                topListener.top();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return this;
    }

    public DraggableView bottom() {
        try {
            parms = (LinearLayout.LayoutParams) getLayoutParams();
            parms.topMargin = parent.getHeight() - getHeight();
            setLayoutParams(parms);
            if (bottomListener != null)
                bottomListener.bottom();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return this;
    }

    public DraggableView left() {
        try {
            parms = (LinearLayout.LayoutParams) getLayoutParams();
            parms.leftMargin = 0;
            setLayoutParams(parms);
//            //canTouch = true;
            if (leftListener != null)
                leftListener.left();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return this;
    }

    public DraggableView right() {
        try {
            parms = (LinearLayout.LayoutParams) getLayoutParams();
            parms.leftMargin = parent.getWidth() - getWidth();
            setLayoutParams(parms);
            if (rightListener != null)
                rightListener.right();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return this;
    }

    public void smoothTop() {
        removeCallbacks(smoothTopRunnable);
        removeCallbacks(smoothBottomRunnable);
        setParams();
        if (parms.topMargin - SPEED < getHeight()) {
            top();
        } else {
            parms.topMargin -= SPEED;
            setLayoutParams(parms);
            postDelayed(smoothTopRunnable, DELAY);
        }
    }

    public void smoothBottom() {
        removeCallbacks(smoothTopRunnable);
        removeCallbacks(smoothBottomRunnable);
        setParams();
        if (parms.topMargin + SPEED > parent.getHeight() - getHeight()) {
            bottom();
        } else {
            parms.topMargin += SPEED;
            setLayoutParams(parms);
            postDelayed(smoothBottomRunnable, DELAY);

        }

    }

    public void smoothLeft() {
        removeCallbacks(smoothRightRunnable);
        removeCallbacks(smoothLeftRunnable);
        setParams();
        if (parms.leftMargin - SPEED < 0) {
            left();
        } else {
            parms.leftMargin -= SPEED;
            setLayoutParams(parms);
            postDelayed(smoothLeftRunnable, DELAY);
        }
    }

    public void smoothRight() {
        removeCallbacks(smoothRightRunnable);
        removeCallbacks(smoothLeftRunnable);
        setParams();
        if (parms.leftMargin + SPEED > parent.getWidth() - getWidth()) {
            right();
        } else {
            parms.leftMargin += SPEED;
            setLayoutParams(parms);
            postDelayed(smoothRightRunnable, DELAY);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (canUse)
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    click = true;
                    removeCallbacks(smoothTopRunnable);
                    removeCallbacks(smoothBottomRunnable);
                    removeCallbacks(smoothRightRunnable);
                    removeCallbacks(smoothLeftRunnable);
                    setParams();
                    if (dragListener != null)
                        dragListener.dragStart(getPositionHorizontal(), getPositionVertical(), getPercentHorizontal(), getPercentVertical());
                    dx = event.getRawX() - parms.leftMargin;
                    dy = event.getRawY() - parms.topMargin;
                    bringToFront();
                }
                break;
                case MotionEvent.ACTION_MOVE: {
                    if (x - dx > 5 || y - dy > 5)
                        click = false;
                    x = event.getRawX();
                    y = event.getRawY();

                    if (dragHorizontal)
                        parms.leftMargin = (int) (x - dx);
                    if (dragVertical)
                        parms.topMargin = (int) (y - dy);
                    setParams();
                    if (dragListener != null)
                        dragListener.move(getPositionHorizontal(), getPositionVertical(), getPercentHorizontal(), getPercentVertical());

                }
                break;
                case MotionEvent.ACTION_UP: {
                    if (!click) {
                        if (dragListener != null)
                            dragListener.dragStop(getPositionHorizontal(), getPositionVertical(), getPercentHorizontal(), getPercentVertical());
                    } else {
                        if (clickListener != null)
                            clickListener.click();
                    }
                }
                break;
            }
        return true;
    }

    private void setParams() {
        if (!canDragOutsideView) {
            parms = (LinearLayout.LayoutParams) getLayoutParams();
            if (parms.leftMargin < 0) {
                parms.leftMargin = 0;
            }
            if (parms.leftMargin > parent.getWidth() - getWidth()) {
                parms.leftMargin = parent.getWidth() - getWidth();
            }
            if (parms.topMargin < 0) {
                parms.topMargin = 0;
            }
            if (parms.topMargin > parent.getHeight() - getHeight()) {
                parms.topMargin = parent.getHeight() - getHeight();
            }
        }
        setLayoutParams(parms);
    }

    private int random(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }

    public interface TopListener {
        void top();
    }

    public interface DragListener {
        void dragStart(int x, int y, int percentHorizontal, int percentVertical);

        void dragStop(int x, int y, int percentHorizontal, int percentVertical);

        void move(int x, int y, int percentHorizontal, int percentVertical);
    }

    public interface ClickListener {
        void click();
    }

    public interface BottomListener {
        void bottom();
    }

    public interface LeftListener {
        void left();
    }

    public interface RightListener {
        void right();
    }
}
