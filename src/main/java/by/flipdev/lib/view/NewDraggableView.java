package by.flipdev.lib.view;

import android.animation.Animator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import java.util.Random;

import by.flipdev.lib.helper.ViewMeter;

/**
 * Created by egormoseevskiy on 23.04.14.
 */
public class NewDraggableView extends FrameLayout {
    private final static int DEF_ANIM_DURATIUON = 200;
    private Context context;
    private View view = null;
    private float dx = 0, dy = 0, x = 0, y = 0;
    private OnAutoMoveListener onAutoMoveListener;
    private DragListener dragListener;
    private OnClickListener onClickListener;
    private View parent;
    private boolean canUse = false;
    private boolean dragVertical = true;
    private boolean dragHorizontal = true;
    private boolean canDragOutsideView = true;
    private boolean click = true;

    public NewDraggableView(Context context) {
        super(context);
        init();
    }

    public NewDraggableView(Context context, View view) {
        super(context);
        this.view = view;
        setMovableView(view);
        init();
    }

    public NewDraggableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NewDraggableView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NewDraggableView setDragListener(DragListener dragListener) {
        this.dragListener = dragListener;
        return this;
    }

    public NewDraggableView setOnAutoMoveListener(OnAutoMoveListener onAutoMoveListener) {
        this.onAutoMoveListener = onAutoMoveListener;
        return this;
    }


    public NewDraggableView setCanUse(boolean canUse) {
        this.canUse = canUse;
        return this;
    }

    public NewDraggableView setDragHorizontal(boolean dragHorizontal) {
        this.dragHorizontal = dragHorizontal;
        return this;
    }

    public NewDraggableView setDragVertical(boolean dragVertical) {
        this.dragVertical = dragVertical;
        return this;
    }

    public NewDraggableView setCanDragOutsideView(boolean canDragOutsideView) {
        this.canDragOutsideView = canDragOutsideView;
        return this;
    }

    public void setMovableView(View view) {
        removeAllViews();
        this.view = view;
        addView(view);
    }

    private void init() {
        context = getContext();
        ViewMeter.measure(this, new ViewMeter.ViewMeterListener() {
            @Override
            public void result(int width, int height) {
                parent = ((View) getParent());
                canUse = true;
            }
        });

    }

    public int getPercentVertical() {
        if (getSizeVertical() != 0)
            return ((getPositionVertical() * 100) / getSizeVertical());
        return 0;
    }

    public int getPercentHorizontal() {
        if (getSizenHorizontal() != 0)
            return ((getPositionHorizontal() * 100) / getSizenHorizontal());
        return 0;
    }

    public int getSizeVertical() {
        if (parent == null)
            parent = ((View) getParent());
        return parent.getHeight() - getHeight();
    }

    public int getSizenHorizontal() {
        if (parent == null)
            parent = ((View) getParent());
        return parent.getWidth() - getWidth();
    }

    public int getPositionVertical() {
        return (int) getTranslationY();
    }

    public int getPositionHorizontal() {
        return (int) getTranslationX();
    }

    public NewDraggableView top() {
        setTranslationY(0);
        return this;
    }

    public NewDraggableView bottom() {
        setTranslationY(parent.getHeight() - getHeight());
        return this;
    }

    public NewDraggableView left() {
        setTranslationX(0);
        return this;
    }

    public NewDraggableView right() {
        setTranslationX(parent.getWidth() - getWidth());
        return this;
    }

    public void smoothTo(int x, int y, int duration) {
        boolean notifyVertical = false, notifyHorizontal = false;
        if (x == -1) x = (int) getTranslationX();
        else notifyVertical = true;
        if (y == -1) y = (int) getTranslationY();
        else notifyHorizontal = true;
        if (duration == -1)
            duration = DEF_ANIM_DURATIUON;
        animate().cancel();
        final boolean finalNotifyVertical = notifyVertical;
        final boolean finalNotifyHorizontal = notifyHorizontal;
        final int finalX = x;
        final int finalY = y;
        animate().translationX(x).translationY(y).setDuration(duration).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (onAutoMoveListener != null) {
                    if (finalNotifyVertical && finalX == 0) {
                        onAutoMoveListener.left();
                    }
                    if (finalNotifyVertical && finalX == parent.getWidth() - getWidth()) {
                        onAutoMoveListener.right();
                    }
                    if (finalNotifyHorizontal && finalY == 0) {
                        onAutoMoveListener.top();
                    }
                    if (finalNotifyHorizontal && finalY == parent.getHeight() - getHeight()) {
                        onAutoMoveListener.bottom();
                    }
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
    }


    public void smoothTop(int duration) {
        smoothTo(-1, 0, duration);
    }

    public void smoothTop() {
        smoothTop(-1);
    }

    public void smoothBottom(int duration) {
        smoothTo(-1, getBottomY(), duration);
    }

    public void smoothBottom() {
        smoothBottom(-1);
    }

    public void smoothLeft(int duration) {
        smoothTo(0, -1, duration);
    }

    public void smoothLeft() {
        smoothLeft(-1);
    }

    public void smoothRight(int duration) {
        smoothTo(getRightX(), -1, duration);
    }

    public void smoothRight() {
        smoothRight(-1);
    }

    public int getBottomY() {
        return parent.getHeight() - getHeight();
    }

    public int getRightX() {
        return parent.getWidth() - getWidth();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (canUse)
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    click = true;
                    animate().cancel();
                    setParams();
                    if (dragListener != null)
                        dragListener.dragStart(getPositionHorizontal(), getPositionVertical(), getPercentHorizontal(), getPercentVertical());
                    dx = event.getRawX() - getTranslationX();
                    dy = event.getRawY() - getTranslationY();
                    bringToFront();
                }
                break;
                case MotionEvent.ACTION_MOVE: {
                    if (x - dx > 5 || y - dy > 5)
                        click = false;
                    x = event.getRawX();
                    y = event.getRawY();

                    if (dragHorizontal)
                        setTranslationX((int) (x - dx));
                    if (dragVertical)
                        setTranslationY((int) (y - dy));
                    setParams();
                    if (dragListener != null)
                        dragListener.move(getPositionHorizontal(), getPositionVertical(), getPercentHorizontal(), getPercentVertical());

                }
                break;
                case MotionEvent.ACTION_UP: {
                    if (!click) {
                        if (dragListener != null)
                            dragListener.dragStop(getPositionHorizontal(), getPositionVertical(), getPercentHorizontal(), getPercentVertical());
                    } else {
                        if (onClickListener != null)
                            onClickListener.onClick(this);
                    }
                }
                break;
            }
        return true;
    }

    private void setParams() {
//        if (!canDragOutsideView) {
//            parms = (LinearLayout.LayoutParams) getLayoutParams();
//            if (parms.leftMargin < 0) {
//                parms.leftMargin = 0;
//            }
//            if (parms.leftMargin > parent.getWidth() - getWidth()) {
//                parms.leftMargin = parent.getWidth() - getWidth();
//            }
//            if (parms.topMargin < 0) {
//                parms.topMargin = 0;
//            }
//            if (parms.topMargin > parent.getHeight() - getHeight()) {
//                parms.topMargin = parent.getHeight() - getHeight();
//            }
//        }
//        setLayoutParams(parms);
    }

    private int random(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }


    public interface DragListener {
        void dragStart(int x, int y, int percentHorizontal, int percentVertical);

        void dragStop(int x, int y, int percentHorizontal, int percentVertical);

        void move(int x, int y, int percentHorizontal, int percentVertical);
    }

    public interface OnAutoMoveListener {
        void right();

        void left();

        void top();

        void bottom();
    }


}
