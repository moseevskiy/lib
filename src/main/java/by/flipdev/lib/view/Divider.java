package by.flipdev.lib.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by egormoseevskiy on 03.10.14.
 */
public class Divider {
    public static View create(Context context, int size, int colorResId) {
        View divider = new View(context);
        divider.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, size));
        divider.setBackgroundColor(context.getResources().getColor(colorResId));
        return divider;
    }
}
