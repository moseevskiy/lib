package by.flipdev.lib.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Region;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

public class LButton extends View {

    private float mDownX;
    private float mDownY;

    private float mRadius;

    private Paint mPaint;
    private Path mPath = new Path();
    private Path mPath2 = new Path();


    private int w, h;

    public LButton(final Context context) {
        super(context);
        init();
    }

    public LButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LButton(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAlpha(100);
    }


    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            mDownX = event.getX();
            mDownY = event.getY();

            ObjectAnimator animator = ObjectAnimator.ofFloat(this, "radius", 0, getHeight() * 3.0f);
            animator.setInterpolator(new AccelerateInterpolator());
            animator.setDuration(1000);

            animator.setRepeatCount(-1);
            animator.start();
        }
        return super.onTouchEvent(event);
    }

    public void setRadius(final float radius) {
        mRadius = radius;
        if (mRadius > 0) {
            RadialGradient radialGradient = new RadialGradient(
                    mDownX,
                    mDownY,
                    mRadius * 3,
                    Color.TRANSPARENT,
                    Color.BLACK,
                    Shader.TileMode.CLAMP
            );
            mPaint.setShader(radialGradient);
        }
        invalidate();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (canvas == null || canvas.getHeight() == 0 || canvas.getWidth() == 0) {
            super.onDraw(canvas);
            return;
        }
        if (w != canvas.getWidth() || h != canvas.getHeight()) {
            w = canvas.getWidth();
            h = canvas.getHeight();
        }
        mPath2.reset();
        mPath2.addCircle(mDownX, mDownY, mRadius, Path.Direction.CW);

        canvas.clipPath(mPath2);

        mPath.reset();
        mPath.addCircle(mDownX, mDownY, mRadius / 3, Path.Direction.CW);

        canvas.clipPath(mPath, Region.Op.DIFFERENCE);

        canvas.drawCircle(mDownX, mDownY, mRadius, mPaint);
    }

}