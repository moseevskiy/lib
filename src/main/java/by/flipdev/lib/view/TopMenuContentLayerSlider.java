package by.flipdev.lib.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import by.flipdev.lib.helper.ViewMeter;
import by.flipdev.lib.helper.convertation.DP;
import by.flipdev.lib.helper.picture.BitmapHelper;


/**
 * Created by egormoseevskiy on 23.04.14.
 */
public class TopMenuContentLayerSlider extends RelativeLayout {

    private static final int DELAY = 1;
    private static int SPEED = 65;
    public boolean stateTop = true;
    Runnable smoothTopRunnable = new Runnable() {
        @Override
        public void run() {
            smoothTop();
        }
    };
    Runnable smoothBottomRunnable = new Runnable() {
        @Override
        public void run() {
            smoothBottom();
        }
    };
    private LinearLayout.LayoutParams contentParams;
    private LayoutParams topParams;
    private RelativeLayout content = null;
    private View contentView = null;
    private View top = null;
    private TopMenuListener listener;
    private Context context;
    private float dy = 0, y = 0;
    private ImageView blocker;
    private boolean canUse;
    private boolean updateBitmap = true;
    private int contentViewBackgroundResourceId = 0;
    private Bitmap currentScreen;

    public TopMenuContentLayerSlider(Context context) {
        super(context);
        init(context);
    }

    public TopMenuContentLayerSlider(View content, View top) {
        super(content.getContext());
        context = getContext();
        setTop(top);
        setContent(content);
    }

    public TopMenuContentLayerSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public static TopMenuContentLayerSlider create(View content, View side) {
        return new TopMenuContentLayerSlider(content, side).top();
    }

    public boolean getStateTop() {
        return stateTop;
    }

    public boolean tougle() {
        if (getStateTop()) {
            smoothBottom();
        } else {
            smoothTop();
        }
        return stateTop;
    }

    public TopMenuContentLayerSlider setTop(View top) {
        this.top = top;
        return this;
    }

    public void setListener(TopMenuListener listener) {
        this.listener = listener;
    }

    public TopMenuContentLayerSlider setContent(View content) {
        this.content = new RelativeLayout(context);
        RelativeLayout.LayoutParams contentParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.content.setLayoutParams(contentParams);
        this.content.addView(content);


        this.blocker = new ImageView(context);
        RelativeLayout.LayoutParams blockerParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        this.blocker.setLayoutParams(blockerParams);
        blocker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                smoothTop();
            }
        });
        this.content.addView(blocker);
        return this;
    }

    public void init(final Context context) {
        this.context = context;
        SPEED = DP.get(context, 20);
        ViewMeter.measure(this, new ViewMeter.ViewMeterListener() {
            @Override
            public void result(int width, int height) {
                contentView = getChildAt(1);
                top = getChildAt(0);
                removeAllViews();
                LinearLayout rail = new LinearLayout(context);
                setContent(contentView);
                if (contentViewBackgroundResourceId != 0)
                    contentView.setBackgroundResource(contentViewBackgroundResourceId);
                rail.addView(content);

                addView(top);
                addView(rail);
                setParams(width, height);
                topParams = (LayoutParams) top.getLayoutParams();
//                topParams.topMargin = -top.getHeight();
                top.setLayoutParams(topParams);
                content.bringToFront();
                hideBlocker();
            }
        });
    }

    public void setContentViewBackgroundResource(int resId) {
        if (contentView != null) {
            this.contentView.setBackgroundResource(resId);
        } else {
            contentViewBackgroundResourceId = resId;
        }
    }

    private void hideBlocker() {
        if (blocker.getVisibility() == VISIBLE) {
            contentView.setVisibility(VISIBLE);

            postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentScreen = null;
                    blocker.setImageBitmap(null);
                    blocker.setVisibility(GONE);
                    blocker.setOnTouchListener(null);
                    content.setOnTouchListener(getOnTouchListener(false));
                }
            }, 100);
        }
    }

    private void showBlocker() {


        if (blocker.getVisibility() == GONE) {
            updateViewBitmap();
            blocker.setImageBitmap(currentScreen);
            blocker.setVisibility(VISIBLE);
            blocker.setOnTouchListener(getOnTouchListener(true));
            contentView.setVisibility(GONE);
            content.setOnTouchListener(null);
        }
    }

    public void updateViewBitmap() {
        blocker.setImageBitmap(null);
        currentScreen = BitmapHelper.getViewBitmap(contentView);
        blocker.setImageBitmap(currentScreen);
    }

    public OnTouchListener getOnTouchListener(final boolean clickEnabled) {
        return new OnTouchListener() {
            public boolean click;

            @SuppressLint("NewApi")
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if (canUse) {
                    switch (event.getAction()) {


                        case MotionEvent.ACTION_DOWN: {
//                        if(!getStateTop())
//                            updateViewBitmap();

                            showTop();
                            if (clickEnabled)
                                click = true;
                            removeCallbacks(smoothTopRunnable);
                            removeCallbacks(smoothBottomRunnable);
                            contentParams = (LinearLayout.LayoutParams) content.getLayoutParams();
                            topParams = (RelativeLayout.LayoutParams) top.getLayoutParams();

                            dy = event.getRawY() - content.getTranslationY();

                        }
                        break;

                        case MotionEvent.ACTION_MOVE: {
                            y = event.getRawY();

                            if (clickEnabled)

                                if (y - dy > 10)
                                    click = false;
                            setParams((int) (y - dy));
                        }
                        break;

                        case MotionEvent.ACTION_UP: {
                            if (clickEnabled && click) {
                                blocker.callOnClick();
                                return true;
                            }
                            int percent = getPercent();
                            if (percent < 50) {
                                smoothTop();
                            }
                            if (percent >= 50) {
                                smoothBottom();
                            }


                        }
                        break;
                    }

                    return true;
                }
                return false;
            }
        };
    }

    @SuppressLint("NewApi")
    private void setParams(int sliderY) {
        if (sliderY > 0 && sliderY < top.getHeight()) {
            content.setTranslationY((y - dy));
        }
        if (sliderY < 0) {
            content.setTranslationY(0);
        }
        if (sliderY > top.getHeight()) {
            content.setTranslationY(top.getHeight());
        }
    }

    private void setParams(int width, int height) {
        contentParams = new LinearLayout.LayoutParams(width, height);
        topParams = new LayoutParams(width, top.getHeight());
        content.setLayoutParams(contentParams);
        top.setLayoutParams(topParams);
    }

    public void setMenuHeight(int height) {
        topParams = (LayoutParams) top.getLayoutParams();
        topParams.height = height;
        top.setLayoutParams(topParams);
        top.setLayoutParams(topParams);
    }

    @SuppressLint("NewApi")
    private int getPercent() {
        int size = top.getHeight();
        int pos = (int) content.getTranslationY();
        return ((pos * 100) / size);
    }

    @SuppressLint("NewApi")
    public TopMenuContentLayerSlider top() {
        content.setTranslationY(0);
        hideTop();
        notifyTop();
        hideBlocker();
        return this;
    }

    @SuppressLint("NewApi")
    public TopMenuContentLayerSlider bottom() {
        content.setTranslationY(top.getHeight());
        notifyBottom();
        showBlocker();
        return this;
    }

    @SuppressLint("NewApi")
    public synchronized void smoothTop() {
//        if(updateBitmap)
//            updateViewBitmap();
//        updateBitmap = false;
        removeCallbacks(smoothTopRunnable);
        removeCallbacks(smoothBottomRunnable);
        if (content.getTranslationY() - SPEED < 0) {
            top();
//            updateBitmap = true;
            stateTop = true;
            return;
        } else {
            content.setTranslationY(content.getTranslationY() - SPEED);
            postDelayed(smoothTopRunnable, DELAY);
        }
    }

    @SuppressLint("NewApi")
    public synchronized void smoothBottom() {

//        if(updateBitmap)
//            updateViewBitmap();
//        updateBitmap = false;
        showTop();
        removeCallbacks(smoothTopRunnable);
        removeCallbacks(smoothBottomRunnable);
        if (content.getTranslationY() + SPEED > top.getHeight()) {
            bottom();
            stateTop = false;
//            updateBitmap = true;
        } else {
            content.setTranslationY(content.getTranslationY() + SPEED);
            postDelayed(smoothBottomRunnable, DELAY);
        }

    }

    private void showTop() {
        if (top.getVisibility() == GONE)
            top.setVisibility(VISIBLE);
    }

    private void hideTop() {
        if (top.getVisibility() == VISIBLE)
            top.setVisibility(GONE);
    }

    private void notifyTop() {
//        if(!stateTop)
        if (listener != null)
            listener.top();
    }

    private void notifyBottom() {
//        if(stateTop)
        if (listener != null)
            listener.bottom();
    }


    public void setCanUse(boolean canUse) {
        this.canUse = canUse;
    }


    public interface TopMenuListener {
        void top();

        void bottom();
    }

}
