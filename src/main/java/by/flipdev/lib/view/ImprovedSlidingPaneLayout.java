//package by.flipdev.lib.view;
//
//import android.content.Context;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.widget.SlidingPaneLayout;
//import android.util.AttributeSet;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.FrameLayout;
//
///**
// * Created by moseevskiy on 17.10.13.
// */
//public class ImprovedSlidingPaneLayout extends SlidingPaneLayout {
//    private Context context;
//    private FrameLayout left;
//    private FrameLayout right;
//    Boolean canOpen = true;
//
//    public ImprovedSlidingPaneLayout(Context context) {
//        super(context);
//        this.context = context;
//        this.left = new FrameLayout(context);
//        this.right = new FrameLayout(context);
//        this.addView(left);
//        this.addView(right);
//    }
//
//    public ImprovedSlidingPaneLayout(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        this.context = context;
//    }
//
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        if (canOpen)
//            return super.onInterceptTouchEvent(ev);
//        else
//            return false;
//    }
//
//    public ImprovedSlidingPaneLayout canOpen(Boolean canOpen) {
//        this.canOpen = canOpen;
//        return this;
//    }
//
//    public ImprovedSlidingPaneLayout makeActionBarSlide(Window window) {
//        ViewGroup decorView = (ViewGroup) window.getDecorView();
//        ViewGroup mainContent = (ViewGroup) decorView.getChildAt(0);
//        decorView.removeView(mainContent);
//        setContentView(mainContent);
//        decorView.addView(this);
//        return this;
//    }
//
//    public ImprovedSlidingPaneLayout setMenuView(View view) {
//        for (int i = 0; i < left.getChildCount(); i++) {
//            left.removeView(left.getChildAt(0));
//        }
//        left.addView(view);
//        return this;
//    }
//
//    public ImprovedSlidingPaneLayout setMenuFragment(FragmentManager fragmentManager, Fragment fragment) {
//        for (int i = 0; i < left.getChildCount(); i++) {
//            left.removeView(left.getChildAt(0));
//        }
//        FrameLayout frameLayout = new FrameLayout(context);
//        fragmentManager.beginTransaction()
//                .add(frameLayout.getId(), fragment)
//                .commit();
//        left.addView(frameLayout);
//        return this;
//    }
//
//    public ImprovedSlidingPaneLayout setContentFragment(FragmentManager fragmentManager, Fragment fragment) {
//        for (int i = 0; i < right.getChildCount(); i++) {
//            right.removeView(right.getChildAt(0));
//        }
//        FrameLayout frameLayout = new FrameLayout(context);
//        fragmentManager.beginTransaction()
//                .add(frameLayout.getId(), fragment)
//                .commit();
//        right.addView(frameLayout);
//        return this;
//    }
//
//    public ImprovedSlidingPaneLayout setContentView(View view) {
//        for (int i = 0; i < right.getChildCount(); i++) {
//            right.removeView(right.getChildAt(0));
//        }
//        right.addView(view);
//        return this;
//    }
//
//    public ImprovedSlidingPaneLayout setMenuWidth(int width) {
//        left.setLayoutParams(new LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT));
//        return this;
//    }
//
//}
